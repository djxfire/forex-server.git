# ForexServer

#### 介绍
ForexGo外汇行情分析应用服务端，服务采用Koa框架实现,通过抓取东方财富网、金十数据实现行情提供，同时提供了简单的社区功能，包括图文帖子的发布、点赞、评论等功能。客户端地址参考：[https://gitee.com/djxfire/foreign-exchange-market---app.git](https://gitee.com/djxfire/foreign-exchange-market---app.git)。


#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/201425_301f8ab7_1308501.png "架构图.png")


#### 安装教程

1. 新建数据库，运行xue_forex.sql
2. npm install
3. 启动Redis
4. 启动RabbitMq
5. 启动行情服务 npm run start-cron
6. 启动资讯服务 npm run start-time
7. 启动web服务 npm run start-app
8. 访问后台服务 http://127.0.0.1:3000/login.html

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
