/*
* @Date: 2021/3/4
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/

export default class {
  constructor(stateCount, matrix, matrixCount) {
    this.matrix = [];
    if (matrix) {
      this.matrix = matrix;
      this.matrixCount = matrixCount;
    } else {
      this.matrix = [];
      this.matrixCount = [];
      for (let i = 0; i < stateCount; i++) {
        let state = [];
        let matrixCount = [];
        for (let j = 0; j < stateCount; j++) {
          state.push(1 / stateCount);
          matrixCount.push(0);
        }
        this.matrix.push(state);
        this.matrixCount.push(matrixCount);
      }
    }
  }

  train(dataSet, iter = 10) {
    for (let k = 0; k < iter; k++) {
      for (let i = 0; i < dataSet.length - 1; i++) {
        for (let j = 1; j < dataSet.length; j++) {
          this.matrixCount[dataSet[i]][dataSet[j]]++;
          if (isNaN(this.matrixCount[dataSet[i]][dataSet[j]])) {
            console.log('isNaN:%s, %s', dataSet[i], dataSet[j]);
            return;
          }
        }
      }
    }
    for (let i = 0; i < this.matrixCount.length; i++) {
      let sum = 0;
      for (let j = 0; j < this.matrixCount[i].length; j++) {
        sum += this.matrixCount[i][j];
      }
      for (let j = 0; j < this.matrixCount[i].length; j++) {
        if (sum === 0) {
          this.matrix[i][j] = 0;
        } else {
          this.matrix[i][j] = this.matrixCount[i][j] / sum;
        }
      }
    }
  }

  cast(state) {
    const nextArr = this.matrix[state];
    let maxIndex = 0;
    let maxValue = Number.MIN_VALUE;
    for (let i = 0; i < nextArr.length; i++) {
      if (nextArr[i] > maxValue) {
        maxValue = nextArr[i];
        maxIndex = i;
      }
    }
    return {
      nextState: maxIndex,
      nextProp: nextArr,
    };
  }
}

