/*
* @Date: 2021/2/12
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import get from '../utils/get';
import redis from '../server/core/db/redis';
import rabbitmq from '../server/core/rabbitmq';
const CronJob = require('cron').CronJob;
const QUOTATION = 'i:1.000001,i:0.399001,i:0.399005,i:0.399006,i:1.000300,i:100.HSI,i:100.HSCEI,i:124.HSCCI,i:100.TWII,i:100.N225,i:100.KOSPI200,i:100.KS11,i:100.STI,i:100.SENSEX,i:100.KLSE,i:100.SET,i:100.PSI,i:100.KSE100,i:100.VNINDEX,i:100.JKSE,i:100.CSEALL,' +
  'i:100.SX5E,i:100.FTSE,i:100.MCX,i:100.AXX,i:100.FCHI,i:100.GDAXI,i:100.RTS,i:100.IBEX,i:100.PSI20,i:100.OMXC20,i:100.BFX,i:100.AEX,i:100.WIG,i:100.OMXSPI,i:100.SSMI,i:100.HEX,i:100.OSEBX,i:100.ATX,i:100.MIB,i:100.ASE,i:100.ICEXI,i:100.PX,i:100.ISEQ,' +
  'i:100.DJIA,i:100.SPX,i:100.NDX,i:100.TSX,i:100.BVSP,i:100.MXX,' +
  'i:100.AS51,i:100.AORD,i:100.NZ50,' +
  'i:100.UDI,i:100.BDI,i:100.CRB,' +
  'i:119.USDNZD,i:119.AUDUSD,i:119.EURUSD,i:119.GBPUSD,i:119.USDCAD,i:119.USDJPY,i:119.USDCHF,i:119.AUDCAD,i:119.AUDCHF,i:119.AUDJPY,i:119.AUDNZD,i:119.CADCHF,i:119.CADJPY,i:119.CHFJPY,i:119.EURAUD,i:119.EURCHF,i:119.EURCAD,i:119.EURGBP,i:119.EURJPY,i:119.EURNZD,i:119.GBPAUD,i:119.GBPCAD,i:119.GBPCHF,i:119.GBPJPY,i:119.GBPNZD,i:119.NZDCAD,i:119.NZDCHF,i:119.NZDJPY,i:119.NZDUSD,' +
  'i:122.XAG,i:122.XAU,i:122.XXAU,'+
  'i:112.BC,i:102.CL00Y,i:102.QM00Y';
async function main() {
  const channel = await rabbitmq.getChannel();
  channel.assertExchange('quotation', 'direct', { durable: false });
  new CronJob('*/3 * * * * *', function () {
    get('http://59.push2.eastmoney.com/api/qt/clist/get', {
      pn: 1,
      pz: 200,
      po: 1,
      ut: 'bd1d9ddb04089700cf9c27f6f7426281',
      fltt: 2,
      invt: 2,
      fid: 'f3',
      fs: QUOTATION,
      fields: 'f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f12,f13,f14,f15,f16,f17,f18,f20,f21,f23,f24,f25,f26,f22,f33,f11,f62,f128,f136,f115,f152,f124,f107',
      _: new Date().getTime(),
    }).then( (response) => {
      const result = JSON.parse(response.text);
      const data = result.data.diff;
      const redisClient = redis.getClient();
      const promises = [];
      const quotations = [];
      for (let key in data) {
        const tm = data[key];
        quotations.push({
          lclose: tm.f2,
          decimal: tm.f1,
          amt: tm.f4,
          pcg: tm.f3,
          code: tm.f12,
          open: tm.f17,
          high: tm.f15,
          close: tm.f2,
          low: tm.f16,
        });
        promises.push(
          new Promise((resolve, reject) => {
            redisClient.hgetall(tm.f12, function(err, obj) {
              if (err || !obj) {
                redisClient.hmset(tm.f12, {
                  lclose: tm.f2,
                  decimal: tm.f1,
                  amt: tm.f4,
                  pcg: tm.f3,
                  code: tm.f12,
                  open: tm.f17,
                  high: tm.f15,
                  close: tm.f2,
                  low: tm.f16,
                });
                reject(err);
                return;
              }
              redisClient.hmset(tm.f12, {
                lclose: obj.close,
                decimal: tm.f1,
                amt: tm.f4,
                pcg: tm.f3,
                code: tm.f12,
                open: tm.f17,
                high: tm.f15,
                close: tm.f2,
                low: tm.f16,
              });
              resolve(1);
            });
          })
        );
      }
      Promise.all(promises).then(async () => {
        try {
          channel.publish('quotation', 'info', Buffer.from(`QUOTATION_PUSH:${JSON.stringify(quotations)}`));
        } catch(error) {
          console.error(error.toString());
        }
      });
    });
  }, null, true);
}
main();

