/*
* @Date: 2021/3/6
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import controller from '../core/controller'
import { get, post } from '../core/route';
import { db, tran, id } from '../core/db/mysql/db';
import Query from '../core/db/mysql/query';
import i18n from "../core/i18n";
import http from '../utils/http';
import cheerio from 'cheerio';
import rabbitmq from "../core/rabbitmq";

let url_0 = 'http://forex.eastmoney.com/a/';

@controller()
export default class market {
  @get('/api/market/bigbank')
  async bigbank() {
    const { p } = this.params;
    try {
      let res = await http.get(`${url_0}/cdhgd_${p}.html`,
        {},
        {
          'Host': 'forex.eastmoney.com'
        },"utf-8");
      const html = res.text;
      const $ = cheerio.load(html, { decodeEntities: false });
      const ret = [];
      $('#newsListContent li').each((i, ele) => {
        let links = $(ele).children('.text');
        if (links.length > 0) {
          const titleDiv = $(links[0]).children('p.title');
          const title = titleDiv.text().trim();
          const info = $(links[0]).children('p.info').text().trim();
          const time = $(links[0]).children('p.time').text().trim();
          const href = titleDiv.children('a').attr('href').trim();
          const id = href.substr(href.lastIndexOf('/') + 1, href.length - href.lastIndexOf('/') - 6);
          ret.push({
            id,
            title,
            info,
            time
          });
        }
      });
      let total = 1;
      const pager = $('#pagerNoDiv a');
      if (pager.length > 0) {
        const val = $(pager[pager.length - 1]).text().trim();
        if (val === '下一页') {
          total = Number($(pager[pager.length - 2]).text().trim());
        } else {
          total = Number(val);
        }
      }
      this.assign('data', ret);
      this.assign('total', total);
      return this.setCode(0).setMsg('获取成功').json();
    } catch(err) {
      console.log('error===>', err);
      return this.setCode(1).setMsg('获取失败').json();
    }
  }

  @get('/api/market/centralbank')
  async centralbank() {
    try {
      const { p } = this.params;
      let res = await http.get(`${url_0}/cyhdt_${p}.html`,
        {},
        {
          'Host': 'forex.eastmoney.com'
        },"utf-8")
      const html = res.text;
      const $ = cheerio.load(html, { decodeEntities: false });
      const ret = [];
      $('#newsListContent li').each((i, ele) => {
        let links = $(ele).children('.text');
        if (links.length > 0) {
          const titleDiv = $(links[0]).children('p.title');
          const title = titleDiv.text().trim();
          const info = $(links[0]).children('p.info').text().trim();
          const time = $(links[0]).children('p.time').text().trim();
          const href = titleDiv.children('a').attr('href').trim();
          const id = href.substr(href.lastIndexOf('/') + 1, href.length - href.lastIndexOf('/') - 6);
          ret.push({
            id,
            title,
            info,
            time
          });
        }
      });
      let total = 1;
      const pager = $('#pagerNoDiv a');
      if (pager.length > 0) {
        const val = $(pager[pager.length - 1]).text().trim();
        if (val === '下一页') {
          total = Number($(pager[pager.length - 2]).text().trim());
        } else {
          total = Number(val);
        }

      }
      this.assign('data', ret);
      this.assign('total', total);
      return this.setCode(0).setMsg('获取成功').json();
    } catch (err) {
      console.log('error===>', err);
      return this.setCode(1).setMsg('获取失败').json();
    }
  }

  @get('/api/market/data')
  async marketData() {
    try {
      const { p } = this.params;
      let res = await http.get(`${url_0}/cjjsj_${p}.html`,
        {},
        {
          'Host': 'forex.eastmoney.com'
        },"utf-8")
      const html = res.text;
      const $ = cheerio.load(html, { decodeEntities: false });
      const ret = [];
      $('#newsListContent li').each((i, ele) => {
        let links = $(ele).children('.text');
        if (links.length > 0) {
          const titleDiv = $(links[0]).children('p.title');
          const title = titleDiv.text().trim();
          const info = $(links[0]).children('p.info').text().trim();
          const time = $(links[0]).children('p.time').text().trim();
          const href = titleDiv.children('a').attr('href').trim();
          const id = href.substr(href.lastIndexOf('/') + 1, href.length - href.lastIndexOf('/') - 6);
          ret.push({
            id,
            title,
            info,
            time
          });
        }
      });
      let total = 1;
      const pager = $('#pagerNoDiv a');
      if (pager.length > 0) {
        const val = $(pager[pager.length - 1]).text().trim();
        if (val === '下一页') {
          total = Number($(pager[pager.length - 2]).text().trim());
        } else {
          total = Number(val);
        }
      }
      this.assign('data', ret);
      this.assign('total', total);
      return this.setCode(0).setMsg('获取成功').json();
    } catch (err) {
      console.log('error===>', err);
      return this.setCode(1).setMsg('获取失败').json();
    }
  }

}
