/*
* @Date: 2021/2/17
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import controller from '../core/controller'
import { get, post } from '../core/route';
import { db, tran, id } from '../core/db/mysql/db';
import Query from '../core/db/mysql/query';
import i18n from "../core/i18n";
import http from '../utils/http';
import cheerio from 'cheerio';
import rabbitmq from "../core/rabbitmq";

let url_0 = 'http://forex.eastmoney.com/a';

@controller()
export default class news {
  @get('/api/forex/news')
  async forexNews() {
    const { p, c } = this.params;
    let res = await http.get(`${url_0}/a${c}_${p}.html`,
      {},
      {
        'Host': 'forex.eastmoney.com'
      },
      'utf-8'
    );
    const html = res.text;
    const $ = cheerio.load(html, { decodeEntities: false });
    const ret = [];
    $('#newsListContent li').each((i, ele) => {
      let links = $(ele).children('a');
      if (links.length > 0) {
        let idlink = $(links[0]).attr('href');
        const id = idlink.substr(idlink.lastIndexOf('/') + 1, idlink.length - idlink.lastIndexOf('/') - 6);
        let title = $(links[0]).children('span.list-content').html();
        let time = $(links[0]).children('span.list-time').html();
        ret.push({
          id,
          title,
          time
        })
      }
    });
    let total = 1;
    const pager = $('#pagerNoDiv a');
    if (pager.length > 0) {
      total = Number($(pager[pager.length - 1]).html());
    }
    this.assign('data', ret);
    this.assign('total', total);
    this.assign('page', p);
    return this.setCode(0).setMsg(i18n.success).json();
  }

  @get('/api/forex/newsDetail')
  async forexNewsDetail() {
    const { id } = this.params;
    let res = await http.get(`${url_0}/${id}.html`,
      {},
      {
        'Host': 'forex.eastmoney.com'
      },"utf-8")
    const html = res.text;
    const $ = cheerio.load(html, { decodeEntities: false });
    const content = [];
    $('#ContentBody p').each((i, ele) => {
      let paragraph = $(ele).text().trim();
      content.push(paragraph);
    });
    const title = $('.newsContent>h1').text().trim();
    content.pop();
    const abstract = $('#ContentBody .b-review').text().trim();
    this.assign('title', title);
    this.assign('abstract', abstract);
    this.assign('content', content);
    return this.setCode(0).setMsg(i18n.success).json();
  }

  @get('/api/newest')
  async getNewest() {
    const response = await http.get('https://www.jin10.com/flash_newest.js', {});
    const res = response.text;
    const resTrim = res.replace(/var\s*newest\s*=/, '').trim();
    const newsData = JSON.parse(resTrim.substr(0, resTrim.length - 1));
    const results = [];
    for (let i = 0; i < newsData.length;i++) {
      if (!newsData[i].data || !newsData[i].data.content) {
        continue;
      }
      if (/<a\s*[^>]*><img\s*[^]*><\/a>/.test(newsData[i].data.content)) {
        continue;
      }
      if (newsData[i].data.content.indexOf('<b>') >= 0) {
        newsData[i].data.bold = true;
        newsData[i].data.content = newsData[i].data.content.replace('<br>', '').replace('</br>', '');
      }
      results.push(newsData[i]);
    }
    this.assign('data', results);
    return this.setCode(0).setMsg('success').json();
  }
}
