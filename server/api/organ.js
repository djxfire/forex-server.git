/*
* @Date: 2021/2/22
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import controller from '../core/controller'
import { get, post } from '../core/route';
import { db, tran, id } from '../core/db/mysql/db';
import Query from '../core/db/mysql/query';
import i18n from "../core/i18n";
import http from "../utils/http";
const cdnjin10 = 'https://cdn.jin10.com';
const eastmoney = 'http://data.eastmoney.com';
@controller()
export default class organ {
  @get('/api/organ/cme1')
  async cme1() {
    let res = await http.get(`${cdnjin10}/data_center/reports/cme_1.json`,
      {
        _: new Date().getTime(),
      },
      {
        'Host': 'cdn.jin10.com'
      },
      'utf-8'
    );
    this.assign('data', JSON.parse(res.text));
    return this.setCode(0).setMsg(i18n.success).json();
  }

  @get('/api/organ/cftc3')
  async cftc3() {
    let res = await http.get(`${cdnjin10}/data_center/reports/cftc_3.json`,
      {
        _: new Date().getTime(),
      }, {
        'Host': 'cdn.jin10.com'
      },
      'utf-8'
      );
    this.assign('data', JSON.parse(res.text));
    return this.setCode(0).setMsg(i18n.success).json();
  }

  @get('/api/organ/cftc4')
  async cftc4() {
    let res = await http.get(`${cdnjin10}/data_center/reports/cftc_4.json`, {
      _: new Date().getTime(),
    }, {
      'Host': 'cdn.jin10.com'
    },
      'utf-8'
    );
    this.assign('data', JSON.parse(res.text));
    return this.setCode(0).setMsg(i18n.success).json();
  }

  @get('/api/data/list')
  async datalist() {
    const { mkt, stat } = this.params;
    let res = await http.get(`${eastmoney}/DataCenter_V3/Chart/cjsj/foreign.ashx`, {
      mkt,
      stat,
      r: Math.random(),
      isxml: false
    }, {
      'Host': 'data.eastmoney.com',
    }, 'gbk');
    this.assign('data', JSON.parse(res.text));
    return this.setCode(0).setMsg(i18n.success).json();
  }
}
