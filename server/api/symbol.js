/*
* @Date: 2021/2/15
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import controller from '../core/controller'
import { get, post } from '../core/route';
import { db, tran, id } from '../core/db/mysql/db';
import Query from '../core/db/mysql/query';
import i18n from "../core/i18n";

@controller()
export default class symbol {
  @get('/api/symbol/list')
  async list() {
    const records = await db(async conn => {
      let symbolQuery = new Query(conn, 'st_symbol');
      return await symbolQuery.where('status', 1).order('addtime desc').select();
    });
    this.assign('data', records);
    return this.setCode(0).setMsg(i18n.success).json();
  }
}
