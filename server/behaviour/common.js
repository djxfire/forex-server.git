import Query from '../core/db/mysql/query'
export function check_captcha(captcha) {
    console.log('captcha===>', this.session.captcha, captcha);
    if(this.session.captcha === captcha) {
        this.session.captcha = undefined
        return true
    }else {
        this.session.captcha = undefined
        return false
    }
}
export async function no_repeat(conn, data, target, table, field) {
    field = field?field:target
    let query = new Query(conn, table)
    let res = await query.where(field, data[target]).find()
    return res ? false : true
}
