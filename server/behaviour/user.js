import Query from '../core/db/mysql/query'
import {db} from '../core/db/mysql/db'
import md5 from 'md5'
export async function checkPassword(username, password) {
    return await db(async conn => {
        try{
            let query = new Query(conn, 'sys_user')
            let res = await query.where('valid', 1).where('username', username).where('or', 'email', username).find()
            if(res && res.password === md5(password)){
                this.session.userid = res.id
                this.session.username = res.username
                this.session.toplevel = res.toplevel
                return true
            }else {
                return false
            }
        }catch(err) {
            console.error(err)
            return false
        }
    })
}