/*
* @Date: 2021/2/1
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import controller from '../core/controller'
import { get,post } from '../core/route';
import { login } from '../decorater/auth';
import { db, tran, id } from '../core/db/mysql/db';
import Query from '../core/db/mysql/query';
import i18n from "../core/i18n";
import rabbitmq from '../core/rabbitmq';

@controller()
export default class symbol {
  @get('/symbol')
  async index() {
    return await this.fetch('symbol')
  }

  @login
  @post('/symbol/doMarkvo')
  async doMarkvo() {
    const { username } = this.session;
    console.log('doMarkvo===>', username);
    try {
      const ch = await rabbitmq.getChannel();
      ch.assertExchange('calMarkvo', 'direct', { durable: false });
      ch.publish('calMarkvo', 'info', Buffer.from(username));
      return this.setCode(0).setMsg(i18n.success).json();
    } catch(err) {
      console.log('error==>', err);
      return this.setCode(1).setMsg(i18n.sys_err).json();
    }

  }

  @login
  @get('/symbol/list')
  async list() {
    const where = this.params;
    const records = await db(async conn => {
      let symbolQuery = new Query(conn, 'st_symbol');
      return await symbolQuery.order('addtime desc').page(
        where.page ? where.page : 1,15, where);
    });
    this.assign('records', records);
    this.assign('where', where);
    return this.setCode(0).setMsg(i18n.success).json();
  }

  @login
  @post('/symbol/add')
  async add() {
    try {
      return await tran(async conn => {
        let QSymbol = new Query(conn, 'st_symbol');
        const form = this.params;
        await QSymbol.insert({
          ...form,
          addtime: new Date(),
        });
        if (Number(form.status) === 1) {
          const ch = await rabbitmq.getChannel();
          ch.assertExchange('product', 'direct', { durable: false });
          ch.publish('product', 'info', Buffer.from(JSON.stringify({ data: form, type: 'add' })));
        }
        return true;
      }) ? this.setCode(0).setMsg(i18n.success).json() : this.setCode(1).setMsg(i18n.sys_err).json();
    } catch(err) {
      console.log('error===>', err);
      return this.setCode(-500).setMsg(i18n.sys_err).json();
    }
  }

  @login
  @post('/symbol/edit')
  async edit() {
    return await tran(async conn => {
      let QSymbol = new Query(conn, 'st_symbol');
      const _form = this.params;
      try{
        delete _form['addtime'];
        await QSymbol.where('id', _form.id).update(_form);
        if (Number(_form.status) === 1) {
          const ch = await rabbitmq.getChannel();
          ch.assertExchange('product', 'direct', { durable: false });
          ch.publish('product', 'info', Buffer.from(JSON.stringify({ data: _form, type: 'edit' })));
        } else {
          if (Number(_form.status) === 1) {
            const ch = await rabbitmq.getChannel();
            ch.assertExchange('product', 'direct', { durable: false });
            ch.publish('product', 'info', Buffer.from(JSON.stringify({ data: _form.id, type: 'delete' })));
          }
        }
        return this.setMsg(i18n.sys_success), true
      }catch(e) {
        console.error(e)
        return this.setMsg(i18n.sys_err), false
      }
    }) ? this.setCode(0).json() : this.setCode(1).json()
  }

  @login
  @post('/symbol/del')
  async del(){
    let id = this.params.id
    return await tran(async conn => {
      let QSymbol = new Query(conn, 'st_symbol');
      try{
        await QSymbol.where('id', id).delete();
        const ch = await rabbitmq.getChannel();
        ch.assertExchange('product', 'direct', { durable: false });
        ch.publish('product', 'info', Buffer.from(JSON.stringify({ data: id, type: 'delete' })));
        return true
      }catch(e) {
        return false
      }
    }) ? this.setCode(0).setMsg(i18n.sys_success).json() : this.setCode(1).setMsg(i18n.sys_err).json();
  }

  @login
  @post('/symbol/status')
  async status() {
    let id = this.params.id;
    return await db(async conn => {
      let QSymbol = new Query(conn, 'st_symbol');
      try {
        const bool = QSymbol.where('id', id).update({ status: this.params.status });
        if (Number(this.params.status) === 1) {
          const ch = await rabbitmq.getChannel();
          ch.assertExchange('product', 'direct', { durable: false });
          ch.publish('product', 'info', Buffer.from(JSON.stringify({ data: id, type: 'add' })));
        } else {
          const ch = await rabbitmq.getChannel();
          ch.assertExchange('product', 'direct', { durable: false });
          ch.publish('product', 'info', Buffer.from(JSON.stringify({ data: id, type: 'delete' })));
        }
        return bool;
      } catch(err) {
        console.log('status error', err);
        return false;
      }
    }) ? this.setCode(0).setMsg(i18n.sys_success).json() : this.setCode(1).setMsg(i18n.sys_err).json();
  }
}
