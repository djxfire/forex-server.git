import config from  '../config'
let default_config = {
    port: 3000,
    debug:true,
    appKey: ['REACT'],
    lang: 'cn',
    session: {
        key: 'REACT:sess',
        maxAge: 8640000,
        overwrite: true,
        httpOnly: true,
        signed: true,
        rolling: false,
        reneew: false
    },
    db: {
        host: 'localhost',
        user: 'root',
        password: '',
        port: '3006',
        database: 'your_database',
    },
    mongodb: {
        host: '127.0.0.1',
        port: 27017
    },
    mail: {
        server: 'QQ',
        auth: {
            user: 'xxx@qq.com',
            pass: 'xxx'
        }
    },
    static_path: [],
    views: {
        root: '',
        extname: '',
        options: {
            autoescape: true
        }
    },
    upload: {
        multipart: true,
        formidable: {
            maxFileSize: 2000 * 1024 * 1024
        },
        path: 'upload',
    },
    rabbitmq: {
      url: 'amqp://admin:admin@localhost:5672',
    },
}

export default {...default_config, ...config}
