export const prefixSymbol = Symbol('prefix')
export default function controller(path) {
    return (target) => {
        path && (target.prototype[[prefixSymbol]] = path.startsWith('/') ? path : '/')
    }
}
