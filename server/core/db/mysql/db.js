import mysql from 'mysql'
import config from '../../config'

let pool = mysql.createPool(config.db)
export function db(_m){
    return new Promise((resolve, reject) => {
        pool.getConnection((err, conn) => {
            if(err) {
                try{
                    console.log('数据库连接断开')
                    reject(err)
                }catch(e) {
                    reject(e)
                }
            }else {
                let promise = _m(conn)
                promise.then(result => {
                    try{
                        conn.release()
                        console.log('数据库连接断开')
                        resolve(result)
                    }catch(e) {
                        reject(e)
                    }

                }).catch(err => {
                    try{
                        conn.release()
                        console.log('数据库连接断开')
                        reject(err)
                    }catch(err){
                        reject(err)
                    }


                })
            }
        })
    })
}
export function id(conn) {
    return new Promise((resolve, reject) => {
        conn.query('SELECT LAST_INSERT_ID() as id',[],(err, result)=> {
            if(err) {
                reject(err)
            }else {
                resolve(result[0].id)
            }
        })
    })
}
export function tran (tran) {
    return new Promise((resolve, reject) => {  //返回promise提供事务成功与失败的接口
        pool.getConnection((err, conn) => {
            if(err) {
                try{
                    conn.release()
                    console.log('数据库连接断开')
                    reject(err)
                }catch(e) {
                    reject(e)
                }
            }else {
                conn.beginTransaction((err) => { //开始事务处理
                    if(err) {
                        try{
                            conn.release()
                            console.log('数据库连接断开, 错误信息：', err);
                            reject(err)
                        }catch(e) {
                            reject(e);
                        }
                    }else {
                        let promise = tran(conn)  //调用事务处理函数
                        promise.then(result => {
                            conn.commit(err => {  //事务处理函数resolve则提交事务
                                if(err) {
                                    try{
                                        conn.release()
                                        console.log('数据库连接断开, 错误信息：', err);
                                        reject(err)
                                    }catch(err){
                                        reject(err)
                                    }
                                }else {
                                    try{
                                        conn.release()
                                        console.log('数据库连接断开, 错误信息：', err);
                                        resolve(result)
                                    }catch(err){
                                        reject(err)
                                    }
                                }
                            })
                        }).catch(err => {
                            try{
                                conn.rollback(() => {  //事务处理函数reject则回滚事务
                                    try{
                                        conn.release()
                                        console.log('数据库连接断开, 错误信息：', err);
                                        reject(err)
                                    }catch(e) {
                                        reject(e)
                                    }
                                })
                            }catch(e) {
                                try{
                                    conn.release()
                                    console.log('数据库连接断开, 错误信息：', err);
                                    reject(err)
                                }catch(err){
                                    reject(err)
                                }
                            }

                        })
                    }
                })
            }
        })
    })
}
export function m () {
    if(arguments.length < 2){
        return Promise.reject('参数错误')
    }
    let conn = arguments[0]
    let sql = arguments[1]
    let args = []
    for(let i = 2;i < arguments.length;i++) {
        args.push(arguments[i])
    }
    return new Promise((resolve,reject) => {
        conn.query(sql, args, (err, result) => {
            if(err) {
                reject(err)
            }else {
                resolve(result)
            }
        })
    })
}
