/*
* @Date: 2021/2/13
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import redis from 'redis';
import config from '../../config';
let isLive = false;
let instance = null;
function createClient() {
  const client = redis.createClient(
    config.redis.port,
    config.redis.host,
    { auth_pass: config.redis.password }
  );
  client.on('ready', function(res) {
    isLive = true;
  });
  client.on('end', function(err) {
    isLive = false;
  });
  client.on('error', function(err) {
    isLive = false;
  });
  client.on('connect', function() {
    isLive = true;
  });
  instance = client;
  return client;
}

function getClient() {
  if (isLive) {
    return instance;
  }
  return createClient();
}

export default {
  getClient,
}
