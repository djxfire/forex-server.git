import config from './config'
import requireDirectory from 'require-directory'
let i18n =  requireDirectory(module, '../i18n')
export default (config.lang && i18n[config.lang]?i18n[config.lang]['default']:i18n['cn']['default'])