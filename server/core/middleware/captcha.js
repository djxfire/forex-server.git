import captcha from 'svg-captcha'

export default async (ctx, next) => {
    let path = ctx.request.path
    if(path === '/captcha') {
        let ary = captcha.create({fontSize:48, width: 100})
        ctx.body = ary.data
        ctx.type = 'image/svg+xml'
        ctx.session.captcha = ary.text
    }else {
        await next()
    }
}