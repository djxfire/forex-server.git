import mailer from 'nodemailer'
import config from '../config'

let transporter = mailer.createTransport(config.mail)
export default () => {
    let sendMail = (to, subject, content) => {
        let option = {
            from : config.mail.auth.user,
            to : to
        }
        option.subject = subject
        option.html = content
        return new Promise((resolve, reject) => {
            transporter.sendMail(option, (err, response) => {
                if(err) {
                    reject(err)
                }else {
                    resolve(response)
                }
            })
        })
    }
    return function (ctx, next) {
        ctx.sendMail = sendMail
        return next()
    }
}