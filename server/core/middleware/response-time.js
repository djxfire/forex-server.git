/**
 * 监听响应时间
 * @param ctx
 * @param next
 * @returns {Promise<void>}
 */
export default async(ctx, next) => {
    console.log(`Process ${ctx.request.method}${ctx.request.url}...`)
    let start = new Date().getTime(),execTime
    await  next()
    execTime = new Date().getTime() - start
    ctx.response.set('X-Response-Time',`${execTime}ms`)
    console.log(`Response time of ${ctx.request.method}${ctx.request.url} is ${execTime}`)
}