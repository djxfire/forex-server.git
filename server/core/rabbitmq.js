/*
* @Date: 2021/2/11
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import config from './config';
import amqp from 'amqplib';
const connection = amqp.connect(config.rabbitmq.url);
async function getChannel() {
  return new Promise((resolve, reject) => {
    connection.then(conn => {
      return conn.createChannel();
    }).then(ch => {
      resolve(ch);
    }).catch(err => {
      console.log('getChannel ===> ', err);
      reject(err);
    });
  });
}

export default {
  getChannel,
}
