export default {
    success: '返回成功',
    sys_success: '操作成功',
    sys_err: '系统错误',
    captcha: '验证码错误',
    auth_error: '您没有访问资源的权利',
    login_fail: '您还没有登录，请先登录',
    user:{
        login:{
            success: '登录成功',
            error   : '用户名或密码错误'
        }
    },
    repeat:{
        username: '该用户名重复,请重试'
    }
}