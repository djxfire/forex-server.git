import $V from './$V';

let $M = {};
$M.min = function(mat,direct){
    //direct:0->行最小值，1->列最小值
    var ret = [];
    if(direct == 0){
        for(var i = 0;i < mat.length;i++){
            ret.push($V.min(mat[i]));
        }
    }else{
        for(var i = 0;i < mat[0].length;i++){
            var min = mat[0][i];
            for(var j = 0;j < mat.length;j++){
                if(mat[j][i] < min){
                    min = mat[j][i];
                }
            }
            ret.push(min);
        }
    }
    return ret;
}

$M.div = function(mat1,mat2){
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    for(var i = 0;i < row;i++){
        var tmp = [];
        for(var j = 0;j < column;j++){
            tmp.push(mat1[i][j]/mat2[i][j]);
        }
        ret.push(tmp);
    }
    return ret;
}

$M.sub = function(mat1,mat2){
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    for(var i = 0;i < row;i++){
        var tmp = [];
        for(var j = 0;j < column;j++){
            tmp.push(mat1[i][j]-mat2[i][j]);
        }
        ret.push(tmp);
    }
    return ret;
}

$M.mul = function(mat1,mat2){
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    for(var i = 0;i < row;i++){
        var tmp = [];
        for(var j = 0;j < column;j++){
            tmp.push(mat1[i][j]*mat2[i][j]);
        }
        ret.push(tmp);
    }
    return ret;
}

$M.add = function(mat1,mat2){
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    for(var i = 0;i < row;i++){
        var tmp = [];
        for(var j = 0;j < column;j++){
            tmp.push(mat1[i][j] + mat2[i][j]);
        }
        ret.push(tmp);
    }
    return ret;
}

$M.add_constant = function(mat1,constant){
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    for(var i = 0;i < row;i++){
        var tmp = [];
        for(var j = 0;j < column;j++){
            tmp.push(mat1[i][j] + constant);
        }
        ret.push(tmp);
    }
    return ret;
}

$M.sub_constant = function(mat1,constant){
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    for(var i = 0;i < row;i++){
        var tmp = [];
        for(var j = 0;j < column;j++){
            tmp.push(mat1[i][j]-constant);
        }
        ret.push(tmp);
    }
    return ret;
}

$M.mul_constant = function(mat1,constant){
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    for(var i = 0;i < row;i++){
        var tmp = [];
        for(var j = 0;j < column;j++){
            tmp.push(mat1[i][j]*constant);
        }
        ret.push(tmp);
    }
    return ret;
}

$M.div_constant = function(mat1,constant){
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    for(var i = 0;i < row;i++){
        var tmp = [];
        for(var j = 0;j < column;j++){
            tmp.push(mat1[i][j]/constant);
        }
        ret.push(tmp);
    }
    return ret;
}

$M.add_vector = function(mat1,vector,direct){
    //direct:0->行，1->列
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    if(direct == 0){
        for(var i = 0;i < row;i++){
            var tmp = [];
            for(var j = 0;j < column;j++){
                tmp.push(mat1[i][j] + vector[i]);
            }
            ret.push(tmp);
        }
    }else{
        for(var i = 0;i < row;i++){
            var tmp = [];
            for(var j = 0;j < column;j++){
                tmp.push(mat1[i][j] + vector[j]);
            }
            ret.push(tmp);
        }
    }
    return ret;
}

$M.sub_vector = function(mat1,vector,direct){
    //direct:0->行，1->列
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    if(direct == 0){
        for(var i = 0;i < row;i++){
            var tmp = [];
            for(var j = 0;j < column;j++){
                tmp.push(mat1[i][j] - vector[i]);
            }
            ret.push(tmp);
        }
    }else{
        for(var i = 0;i < row;i++){
            var tmp = [];
            for(var j = 0;j < column;j++){
                tmp.push(mat1[i][j] - vector[j]);
            }
            ret.push(tmp);
        }
    }
    return ret;
}

$M.mul_vector = function(mat1,vector,direct){
    //direct:0->行，1->列
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    if(direct == 0){
        for(var i = 0;i < row;i++){
            var tmp = [];
            for(var j = 0;j < column;j++){
                tmp.push(mat1[i][j] * vector[i]);
            }
            ret.push(tmp);
        }
    }else{
        for(var i = 0;i < row;i++){
            var tmp = [];
            for(var j = 0;j < column;j++){
                tmp.push(mat1[i][j] * vector[j]);
            }
            ret.push(tmp);
        }
    }
    return ret;
}

$M.div_vector = function(mat1,vector,direct){
    //direct:0->行，1->列
    var row = mat1.length;
    var column = mat1[0].length;
    var ret = [];
    if(direct == 0){
        for(var i = 0;i < row;i++){
            var tmp = [];
            for(var j = 0;j < column;j++){
                tmp.push(mat1[i][j] / vector[i]);
            }
            ret.push(tmp);
        }
    }else{
        for(var i = 0;i < row;i++){
            var tmp = [];
            for(var j = 0;j < column;j++){
                tmp.push(mat1[i][j] / vector[j]);
            }
            ret.push(tmp);
        }
    }
    return ret;
}

$M.max = function(mat,direct){
    //direct:0->行最大，1->列最大
    var ret = [];
    if(direct == 0){
        for(var i = 0;i < mat.length;i++){
            ret.push($V.max(mat[i]));
        }
    }else{
        for(var i = 0;i < mat[0].length;i++){
            var max = mat[0][i];
            for(var j = 0;j < mat.length;j++){
                if(mat[j][i] > max){
                    max = mat[j][i];
                }
            }
            ret.push(max);
        }
    }
    return ret;
}

$M.subm = function(mat,index,direct){
    //direct:0->取行，1->取列
    var ret = [];
    if(direct == 0){
        for(var i = 0;i < index.length;i++){
            ret.push(mat[index[i]]);
        }
    }else{
        var mlen = mat[0].length;
        for(var i = 0;i < mlen;i++){
            var tmp = [];
            for(var j = 0;j < index.length;j++){
                tmp.push(mat[i][index[j]]);
            }
            ret.push(tmp);
        }
    }
    return ret;
}

$M.mean = function(mat,direct){
    //direct:0->取行,1->取列
    var ret = [];
    if(direct == 0){
        for(var i = 0;i < mat.length;i++){
            ret.push($V.mean(mat[i]));
        }
    }else{
        var columns = mat[0].length;
        for(var i = 0;i < columns;i++){
            var sum = 0;
            for(var j = 0;j < mat.length;j++){
                sum = sum + mat[j][i];
            }
            ret.push(sum/mat.length);
        }
    }
    return ret;

}

$M.column = function(mat,index){
    var ret = [];
    for(var i = 0;i < mat.length;i++){
        ret.push(mat[i][index]);
    }
    return ret;
}

$M.row = function(mat,index){
    return mat[index];
}

export default $M;
