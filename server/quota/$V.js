let $V = {};
$V.add = function(v1, v2){
  var result = [];
  if(v1.length == v2.length){
    for(var i = 0;i < v1.length;i++){
      result.push(parseFloat(v1[i]) +parseFloat(v2[i]));
    }
  }
  return result;
}
$V.add_constant = function(v, constant){
  var result = [];
  for(var i = 0;i < v.length;i++){
    result.push(parseFloat(v[i]) + constant);
  }
  return result;
}
$V.sub = function(v1, v2){
  var result = [];
  if(v1.length == v2.length){
    for(var i = 0;i < v1.length;i++){
      result.push(parseFloat(v1[i]) - parseFloat(v2[i]));
    }
  }
  return result;
}
$V.sub_constant = function(v, constant){
  var result = [];
  for(var i = 0;i < v.length;i++){
    result.push(parseFloat(v[i]) - constant);
  }
  return result;
}

$V.mul = function(v1, v2){
  var result = [];
  if(v1.length == v2.length){
    for(var i = 0;i < v1.length;i++){
      result.push(parseFloat(v1[i])*parseFloat(v2[i]));
    }
  }
  return result;
}
$V.mul_constant = function(v, constant){
  var result = [];
  for(var i = 0;i < v.length;i++){
    result.push(parseFloat(v[i]) * constant);
  }
  return result;
}

$V.div = function(v1, v2){
  var result = [];
  if(v1.length == v2.length){
    for(var i = 0;i < v1.length;i++){
      result.push(parseFloat(v1[i])/parseFloat(v2[i]));
    }
  }
  return result;
}
$V.div_constant = function(v, constant){
  var result = [];
  for(var i = 0;i < v.length;i++){
    result.push(parseFloat(v[i])/constant);
  }
  return result;
}

$V.subv = function(v1, start, end){
  var result = [];
  for(var i = start;i < end;i++){
    result.push(v1[i]);
  }
  return result;
}
$V.set_by_index = function(v, index, value){
  for(var i = 0;i < index.length;i++){
    v[index[i]] = value;
  }
  return v;
}
$V.copy_by_index = function(v, start, value){
  var result = $V.array_copy(v);
  for(var i = 0;i < value.length;i++){
    result[i+start] = value[i];
  }
  return result;
}
$V.array_copy = function(v){
  var result = [];
  for (let i = 0; i < v.length; i++){
    result.push(v[i]);
  }
  return result;
}
$V.copy_by_range = function(v, range, value){
  var result = $V.array_copy(v);

  for(var i = 0;i < range.length;i++){
    result[range[i]] = value[i];
  }
  return result;
}
$V.sum = function(v){
  var result = 0;
  for(var i = 0;i < v.length;i++){
    result += parseFloat(v[i]);
  }
  return result;
}
$V.sum_by_index = function(v, start, end){
  var result = 0;
  for(var i = start;i < end;i++){
    result += parseFloat(v[i]);
  }
  return result;

}
$V.range = function(length){
  var result = [];
  for(var i = 0;i < length;i++){
    result.push(i);
  }
  return result;
}
$V.arange = function(start, length){
  var result = [];
  for(var i = start;i < length;i++){
    result.push(i);
  }
  return result;
}
$V.init = function(length, value){
  var result = [];
  for(var i = 0;i < length;i++){
    result.push(value);
  }
  return result;
}
$V.cumsum = function(v1){
  var result = [];
  result[0] = parseFloat(v1[0]);
  for(var i = 1;i < v1.length;i++){
    result.push(result[i-1] + parseFloat(v1[i]));
  }
  return result;
}
$V.where = function(v, oper, value){
  var index = [];
  for(var i = 0;i < v.length;i++){
    if(eval("v[i]" + oper + value)){
      index.push(i);
    }
  }
  return index;

}
$V.vmax = function(v){
  var index = 0;
  var max = parseFloat(v[0]);
  for(var i = 1;i < v.length;i++){
    if(v[i] > max){
      max = parseFloat(v[i]);
      index = i;
    }
  }
  var result = [];
  result.push(index);
  result.push(max);
  return result;
}
$V.vmin = function(v){
  var index = 0;
  var min = parseFloat(v[0]);
  for(var i = 1;i < v.length;i++){
    if(v[i] <  min){
      min = parseFloat(v[i]);
      index = i;
    }
  }
  var result = [];
  result.push(index);
  result.push(min);
  return result;
}
$V.max = function(v){
  var result = $V.vmax(v);
  return result[1];
}
$V.argmax = function(v){
  var result = $V.vmax(v);
  return result[0];
}
$V.min = function(v){
  var result = $V.vmin(v);
  return result[1];
}
$V.argmin = function(v){
  var result = $V.vmin(v);
  return result[0];
}

$V.mean = function(v){
  var result = 0;
  for(var i = 0;i < v.length;i++){
    result = result +  parseFloat(v[i]);
  }
  return result/v.length;
}
$V.abs = function(v){
  var result = [];
  for(var i = 0;i < v.length;i++){
    result[i] = Math.abs(v[i]);
  }
  return result;
}
$V.maximum = function(v){
  var result = [];
  var count = v[0].length;
  console.log(count)
  for(var j = 0;j < count;j++){
    var max = v[0][j];
    for(var i = 1;i < v.length;i++){
      if(v[i][j] > max){
        max = v[i][j];
      }
    }
    result.push(max);
  }
  return result;
}
$V.square = function(v){
  var result = [];
  for(var i = 0;i < v.length;i++){
    result.push(parseFloat(v[i]) * parseFloat(v[i]));
  }
  return result;
}
$V.sqrt = function(v){
  var result = [];
  for(var i =0; i < v.length;i++){
    result.push(Math.sqrt(parseFloat(v[i])))
  }
  return result;
}
$V.std = function(v){
  var temp = $V.square($V.sub_constant(v, $V.mean(v)));
  var tempsum = $V.sum(temp);
  return Math.sqrt(tempsum/v.length);
}
$V.vector_of_index = function(v, index){
  var result = [];
  for(var i = 0;i < index.length;i++){
    result.push(v[index[i]]);
  }
  return result;

}
$V.compare = function(v1, v2, oper){
  var result = [];
  for(var i = 0;i < v1.length;i++){
    if(eval("v1[i]" + oper + "v2[i]")){
      result.push(1);
    }else{
      result.push(0);
    }
  }
  return result;
}
export default $V;
