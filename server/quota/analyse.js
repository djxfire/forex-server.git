import ad from './ad';
import ma from './ma';
import arbr from './arbr';
import aroon from './aroon';
import bias from './bias';
import cci from './cci';
import cmo from './cmo';
import cr from './cr';
import cv from './cv';
import dma from './dma';
import dmi from './dmi';
import dpo from './dpo';
import emv from './emv';
import env from './env';
import forceIndex from './forceIndex';
import ema from './ema'
import kdj from './kdj';
import macd from './macd';
import mfi from './mfi';
import mike from './mike';
import mtm from './mtm';
import nvi from './nvi';
import obv from './obv';
import psy from './psy';
import pvi from './pvi';
import roc from './roc';
import rsi from './rsi';
import rvi from './rvi';
import sar from './sar';
import trix from './trix';
import vhf from './vhf';
import vr from './vr';
import wad from './wad';
import wms from './wms';
import wvad from './wvad';

import $V from './$V';
function adAnalyse (data) {
    let adv = ad(data.high, data.low, data.close, data.volume);
    let ma6 = ma(adv, 6);
    let diff = $V.sub(adv, ma6);
    let adAn = [0];
    let adM6An = [0];
    for (let i = 1; i < adv.length; i++) {
        if (adv[i] > adv[i - 1]) {
            adAn.push(1);
        } else if (adv[i] < adv[i - 1]) {
            adAn.push(2)
        } else {
            adAn.push(0);
        }
        if (diff[i] > 0 && diff[i - 1] < 0) {
            adM6An.push(1)
        } else if (diff < 0 && diff[i - 1] > 0) {
            adM6An.push(2);
        } else if (diff[i] > 0 && diff[i] > diff[i - 1]) {
            adM6An.push(3);
        } else if (diff[i] > 0 && diff[i] < diff[i - 1]) {
            adM6An.push(4);
        } else if (diff[i] < 0 && diff[i] > diff[i - 1]) {
            adM6An.push(5);
        } else if (diff[i] < 0 && diff[i] < diff[i - 1]) {
            adM6An.push(6);
        } else {
            adM6An.push(0);
        }
    }
    return { adAn, adM6An };
}

function arbrAnalyse(data, length = 26) {
    let { ar, br } = arbr(data.open, data.high, data.low, data.close, length);
    let arAn = [0];
    let brAn = [0];
    for (let i = 1; i < ar.length; i++) {
        if (ar[i] > 80 && ar[i] < 120) {
            arAn.push(1);
        } else if (ar[i] > 150) {
            arAn.push(2);
        } else if (ar[i] > 120 && ar[i] < 150) {
            if (ar[i] > ar[i - 1]) {
                arAn.push(3);
            } else {
                arAn.push(4);
            }
        } else if (ar[i] < 60) {
            arAn.push(5);
        } else if (ar[i] > 60 && ar[i] < 80) {
            if (ar[i] > ar[i - 1]) {
                arAn.push(6);
            } else {
                arAn.push(7);
            }
        } else {
            arAn.push(0);
        }
        if (br[i] > 70 && br[i] < 150) {
            brAn.push(1);
        } else if (br[i] > 400) {
            brAn.push(2);
        } else if (br[i] < 400 && br[i] > 150) {
            if (br[i] > br[i - 1]) {
                brAn.push(3);
            } else {
                brAn.push(4);
            }
        } else if (br[i] < 50) {
            brAn.push(5);
        } else if (br[i] > 50 && br[i] < 70) {
            if (br[i] > br[i - 1]) {
                brAn.push(6);
            } else {
                brAn.push(7);
            }
        } else {
            brAn.push(8);
        }
    }
    return { arAn, brAn };
}

function aroonAnalyse(data, length = 20) {
    let { uparoon, downaroon } = aroon(data.high, data.low, length);
    let upAn = [0];
    let downAn = [0];
    for (let i = 1; i < uparoon.length; i++) {
        if (uparoon[i] > 70) {
            upAn.push(1);
        } else if (uparoon[i] < 30) {
            upAn.push(2);
        } else if (uparoon[i] > 30 && uparoon[i] < 70) {
            if (uparoon[i] > uparoon[i - 1]) {
                upAn.push(3);
            } else {
                upAn.push(4);
            }
        } else {
            upAn.push(0);
        }
        if (downaroon[i] > 70) {
            downAn.push(1);
        } else if (downaroon[i] < 30) {
            downAn.push(2);
        } else if (downaroon[i] > 30 && downaroon[i] < 70) {
            if (downaroon[i] > downaroon[i - 1]) {
                downAn.push(3);
            } else {
                downAn.push(4);
            }
        } else {
            downAn.push(0);
        }
    }
    return { upAn, downAn }
}

function biasAnalyse(data) {
    let bias6 = bias(data.close, 6);
    let biasAn = [0]
    for (let i = 1; i < bias6.length; i++) {
        if (bias6[i] > 0.05) {
            biasAn.push(1)
        } else if (bias6[i] < -0.05) {
            biasAn.push(2);
        } else if (bias6[i] > bias6[i - 1]) {
            biasAn.push(3)
        } else if (bias6[i] < bias6[i - 1]) {
            biasAn.push(4);
        } else {
            biasAn.push(0);
        }
    }
    return biasAn;
}

function cciAnalyse(data) {
    let cciv = cci(data.high, data.low, data.close, 12);
    let cciAn = [0];
    for (let i = 1; i < cciv.length; i++) {
        if (cciv[i] > 100 && cciv[i - 1] < 100) {
            cciAn.push(1);
        } else if (cciv[i] < 100 && cciv[i - 1] > 0) {
            cciAn.push(2);
        } else if (cciv[i - 1] > 100 && cciv[i] > cciv[i - 1]){
            cciAn.push(3);
        } else if (cciv[i - 1] > 100 && cciv[i] < cciv[i - 1]) {
            cciAn.push(4);
        } else if (cciv[i - 1] > -100 && cciv[i] < -100) {
            cciAn.push(5);
        } else if (cciv[i - 1] < -100 && cciv[i] > -100) {
            cciAn.push(6);
        } else if (cciv[i - 1] < -100 && cciv[i] < cciv[i - 1 ]) {
            cciAn.push(7);
        } else if (cciv[i - 1] < -100 && cciv[i] > cciv[i - 1]) {
            cciAn.push(8);
        } else if (cciv[i] > -100 && cciv[i] < 100) {
            if (cciv[i] > cciv[i - 1]) {
                cciAn.push(9);
            } else {
                cciAn.push(10);
            }
        } else {
            cciAn.push(0);
        }
    }
    return cciAn;
}

function cmoAnalyse(data) {
    let cmoV = cmo(data.close, 12);
    let cmoAn = [0];
    for (let i = 1; i < cmoV.length;i++) {
        if (cmoV[i] > 0 && cmoV[i - 1] < 0) {
            cmoAn.push(1);
        } else if (cmoV[i] < 0 && cmoV[i - 1] > 0) {
            cmoAn.push(2);
        } else if (cmoV[i] > 50) {
            cmoAn.push(3)
        } else if (cmoV[i] < -50) {
            cmoAn.push(4);
        } else if (cmoV[i] > 0 && cmoV[i] < 50) {
            if (cmoV[i] > cmoV[i - 1]) {
                cmoAn.push(5);
            } else {
                cmoAn.push(6);
            }
        } else if (cmoV[i] < 0 && cmoV[i] > -50) {
            if (cmoV[i] > cmoV[i - 1]) {
                cmoAn.push(7);
            } else {
                cmoAn.push(8);
            }
        } else {
            cmoAn.push(9);
        }
    }
    return cmoAn;
}

function crAnalyse(data) {
    let crV = cr(data.high, data.low, data.close, 12);
    let crAn = [0];
    for (let i = 0; i < crV.length; i++) {
        if (crV[i] > 300 && crV[i - 1] < 300) {
            crAn.push(1);
        } else if (crV[i] < 300 && crV[i - 1] > 300) {
            crAn.push(2);
        } else if (crV[i] < 40 && crV[i - 1] > 40){
            crAn.push(3);
        } else if (crV[i] > 40 && crV[i - 1] < 40) {
            crAn.push(4);
        }else if (crV[i] > 125 && crV[i] < 300) {
            if (crV[i] > crV[i - 1]) {
                crAn.push(5);
            } else {
                crAn.push(6);
            }
        } else if (crV[i] < 75 && crV[i] > 40) {
            if (crV[i] > crV[i - 1]) {
                crAn.push(7);
            } else {
                crAn.push(8);
            }
        } else if (crV[i] > 75 && crV[i] < 125) {
            if (crV[i] > crV[i - 1]) {
                crAn.push(9);
            } else {
                crAn.push(10);
            }
        } else {
            crAn.push(0);
        }
    }
    return crAn;
}

function dmaAnalyse(data) {
    const { dmaValue, amaValue } = dma(data.close, 10, 20, 5);
    const dmaAn = [0];
    for (let i = 1; i < dmaValue.length; i++) {
        if (dmaValue[i] > amaValue[i] && dmaValue[i - 1] < amaValue[i - 1]) {
            dmaAn.push(1);
        } else if (dmaValue[i] < amaValue[i] && dmaValue[i - 1] > amaValue[i - 1]) {
            dmaAn.push(2);
        } else if (dmaValue[i] > amaValue[i] && dmaValue[i - 1] > amaValue[i - 1]) {
            dmaAn.push(3);
        } else if (dmaValue[i] < amaValue[i] && dmaValue[i - 1] < amaValue[i - 1]) {
            dmaAn.push(4);
        } else {
            dmaAn.push(0);
        }
    }
    return dmaAn;
}

function dmiAnalyse(data) {
    let { PosDI, NegDI, ADX } = dmi(data.high, data.low, data.close, 14);
    let dmiAn1 = [0];
    let dmiAn2 = [0];
    for (let i = 1; i < PosDI.length;i++){
        if (PosDI[i] > NegDI[i] && PosDI[i - 1] < NegDI[i - 1]) {
            dmiAn1.push(1);
        } else if (PosDI[i] < NegDI[i] && PosDI[i - 1] > NegDI[i - 1]) {
            dmiAn1.push(2);
        } else if (PosDI[i] > NegDI[i]) {
            dmiAn1.push(3);
        } else if (PosDI[i] < NegDI[i]) {
            dmiAn1.push(4);
        } {
            dmiAn1.push(0);
        }
        if (ADX[i] > 50 && ADX[i - 1] > 50 && ADX[i] < ADX[i - 1]) {
            dmiAn2.push(1);
        } else if (ADX[i] < 30 && ADX[i - 1] < 30){
            dmiAn2.push(2);
        } else if (ADX[i - 1] < 50 && ADX[i] > 50) {
            dmiAn2.push(3);
        } else if (ADX[i - 1] < 80 && ADX[i] > 80) {
            dmiAn2.push(4);
        } else if (ADX[i - 1] > 50 && ADX[i] < 50) {
            dmiAn2.push(5);
        } else if (ADX[i - 1] > ADX[i]) {
            dmiAn2.push(6);
        } else {
            dmiAn2.push(0);
        }
    }
    return { dmiAn1, dmiAn2 };
}

function dpoAnalyse(data) {
    let dpoV = dpo(data.close, 14);
    let dpoMa = ma(dpoV, 6);
    let dpoAn = [0];
    let dpoMaAn = [0];
    for (let i = 1; i < dpoV.length; i++) {
        if (dpoV[i] > 0 && dpoV[i - 1] < 0) {
            dpoAn.push(1);
        } else if (dpoV[i] < 0 && dpoV[i - 1] > 0) {
            dpoAn.push(2);
        } else if (dpoV[i] > 0 && dpoV[i - 1] > 0 && dpoV[i] > dpoV[i - 1]) {
            dpoAn.push(3);
        } else if (dpoV[i] < 0 && dpoV[i - 1] < 0 && dpoV[i] < dpoV[i - 1]) {
            dpoAn.push(4);
        } else if (dpoV[i] > 0 && dpoV[i - 1] > 0 && dpoV[i] < dpoV[i - 1]) {
            dpoAn.push(5);
        } else if (dpoV[i] < 0 && dpoV[i - 1] < 0 && dpoV[i] > dpoV[i - 1]) {
            dpoAn.push(6);
        } else {
            dpoAn.push(0);
        }
        if (dpoV[i] > dpoMa[i] && dpoV[i - 1] < dpoMa[i - 1]) {
            dpoMaAn.push(1);
        } else if (dpoV[i] < dpoMaAn[i] && dpoV[i - 1] > dpoMaAn[i]) {
            dpoMaAn.push(2);
        } else if (dpoV[i] > dpoMaAn[i] && dpoV[i - 1] > dpoMaAn[i - 1]) {
            dpoMaAn.push(3);
        } else if (dpoV[i] < dpoMaAn[i] && dpoV[i - 1] < dpoMaAn[i - 1]) {
            dpoMaAn.push(4);
        } else {
            dpoMaAn.push(0);
        }
    }
    return { dpoAn, dpoMaAn };
}

function emvAnalyse(data) {
    const { EMVValue, MAEMVVAlue } = emv(data.high, data.low, data.volume, 12, 6);
    let emvAn = [0];
    let emvMaAn = [0];
    for (let i = 0; i < EMVValue.length; i++) {
        if (EMVValue[i] > 0 && EMVValue[i - 1] < 0) {
            emvAn.push(1);
        } else if (EMVValue[i] < 0 && EMVValue[i - 1] > 0) {
            emvAn.push(2);
        } else if (EMVValue[i] > 0 && EMVValue[i - 1] > 0) {
            if (EMVValue[i] > EMVValue[i - 1]) {
                emvAn.push(3);
            } else {
                emvAn.push(4);
            }
        } else if (EMVValue[i] < 0 && EMVValue[i - 1] < 0) {
            if (EMVValue[i] > EMVValue[i - 1]) {
                emvAn.push(5);
            } else {
                emvAn.push(6);
            }
        } else {
            emvAn.push(0);
        }

        if (MAEMVVAlue[i] > EMVValue[i] && MAEMVVAlue[i - 1] < EMVValue[i - 1]) {
            emvMaAn.push(1);
        } else if (MAEMVVAlue[i] < EMVValue[i] && MAEMVVAlue[i - 1] > EMVValue[i - 1]) {
            emvMaAn.push(2);
        } else if (MAEMVVAlue[i] > EMVValue[i] && MAEMVVAlue[i - 1] > EMVValue[i - 1]) {
            emvMaAn.push(3);
        } else if (MAEMVVAlue[i] < EMVValue[i] && MAEMVVAlue[i - 1] < EMVValue[i - 1]) {
            emvMaAn.push(4);
        } else {
            emvMaAn.push(0);
        }
    }

    return { emvAn, emvMaAn };
}

function forceIndexAnalyse(data) {
    let forceIndexV = forceIndex(data.close, data.volume);
    let ema2 = ema(forceIndexV, 2);
    let ema3 = ema(forceIndexV, 13);
    let ema2An = [0];
    let ema3An = [0];
    for (let i = 1; i < ema2.length; i++) {
        if (ema2[i] > 0 && ema2[i - 1] < 0) {
            ema2An.push(1);
        } else if (ema2[i] < 0 && ema2[i - 1] > 0){
            ema2An.push(2);
        } else if (ema2[i] > 0 && ema2[i - 1] > 0) {
            if (ema2[i] > ema2[i - 1]) {
                ema2An.push(3);
            } else {
                ema2An.push(4);
            }
        } else if (ema2[i] < 0 && ema2[i - 1] < 0) {
            if (ema2[i] > ema2[i - 1]) {
                ema2An.push(5);
            } else {
                ema2An.push(6);
            }
        } else {
            ema2An.push(0);
        }

        if (ema3[i] > 0 && ema3[i - 1] < 0) {
            ema3An.push(1);
        } else if (ema3[i] < 0 && ema3[i - 1] > 0){
            ema3An.push(2);
        } else if (ema3[i] > 0 && ema3[i - 1] > 0) {
            if (ema3[i] > ema3[i - 1]) {
                ema3An.push(3);
            } else {
                ema3An.push(4);
            }
        } else if (ema3[i] < 0 && ema3[i - 1] < 0) {
            if (ema3[i] > ema3[i - 1]) {
                ema3An.push(5);
            } else {
                ema3An.push(6);
            }
        } else {
            ema3An.push(0);
        }
    }
    return { ema2An, ema3An };
}

function kdjAnalyse(data) {
    let { KValue, DValue, JValue } = kdj(data.high, data.low, data.close, 14, 3, 3, 3);
    let kdj1 = [0];
    let kdj2 = [0];
    for (let i = 0; i < DValue.length; i++) {
        if (DValue[i] > 70 && DValue[i - 1] < 70) {
            kdj1.push(1);
        } else if (DValue[i] < 70 && DValue[i - 1] > 70) {
            kdj1.push(2);
        } else if (DValue[i] > 30 && DValue[i - 1] < 30) {
            kdj1.push(3);
        } else if (DValue[i] < 30 && DValue[i - 1] > 30) {
            kdj1.push(4);
        } else if (DValue[i] > DValue[i - 1]) {
            kdj1.push(5)
        } else if (DValue[i] < DValue[i - 1]) {
            kdj1.push(6);
        } else {
            kdj1.push(7);
        }

        if (KValue[i] > DValue[i] && KValue[i - 1] < DValue[i - 1]) {
            kdj2.push(1);
        } else if (KValue[i] < DValue[i] && KValue[i - 1] > DValue[i - 1]) {
            kdj2.push(2);
        } else if (KValue[i] > DValue[i] && KValue[i - 1] > DValue[i - 1]) {
            kdj2.push(3);
        } else if (KValue[i] < DValue[i] && KValue[i - 1] < DValue[i - 1]) {
            kdj2.push(4);
        } else {
            kdj2.push(5);
        }
    }
    return { kdj1, kdj2 };
}

function macdAnalyse(data) {
    const { DIF, DEA, BAR } = macd(data.close, 12, 26 ,9);
    let macdAn = [0];
    for (let i = 0; i < DIF.length; i++) {
        if(DIF[i] > DEA[i] && DIF[i - 1] < DEA[i - 1]) {
            macdAn.push(1);
        } else if (DIF[i] < DEA[i] && DIF[i - 1] > DEA[i - 1]) {
            macdAn.push(2);
        } else if(BAR[i] > 0 && BAR[i - 1] < 0) {
            macdAn.push(6);
        } else if(BAR[i] < 0 && BAR[i - 1] > 0){
            macdAn.push(7)
        } else if (BAR[i] > BAR[i - 1]){
            macdAn.push(8);
        }else if (DIF[i] > 0 && DEA[i] > 0) {
            macdAn.push(3);
        } else if (DIF[i] < 0 && DEA[i] < 0) {
            macdAn.push(4);
        } else {
            macdAn.push(5)
        }
    }
    return macdAn;
}

function mfiAnalyse(data) {
    const MFIValue = mfi(data.high, data.low, data.close, data.volume, 6);
    const mfiAn = [0];
    for (let i = 1; i < MFIValue.length; i++) {
        if (MFIValue[i] > 20 && MFIValue[i - 1] < 20) {
            mfiAn.push(1);
        } else if (MFIValue[i] < 20 && MFIValue[i - 1] > 20) {
            mfiAn.push(2);
        } else if (MFIValue[i] > 80 && MFIValue[i - 1] < 80) {
            mfiAn.push(3);
        } else if (MFIValue[i] < 80 && MFIValue[i - 1] > 80) {
            mfiAn.push(4);
        } else if(MFIValue[i] > 20 && MFIValue[i] < 80) {
            if(MFIValue[i] > MFIValue[i - 1]) {
                mfiAn.push(5);
            } else {
                mfiAn.push(6);
            }
        } else if (MFIValue[i] < 20){
            mfiAn.push(7);
        } else if (MFIValue[i] > 80) {
            mfiAn.push(8);
        } else {
            mfiAn.push(0);
        }
    }
    return mfiAn;
}

function mtmAnalyse(data) {
    const mtmValue = mtm(data.close, 12);
    let mtmAn = [0];
    for(let i = 1; i < mtmValue.length; i++) {
        if (mtmValue[i] > 0 && mtmValue[i - 1] < 0) {
            mtmAn.push(1);
        } else if (mtmValue[i] < 0 && mtmValue[i - 1] > 0) {
            mtmAn.push(2);
        } else if (mtmValue[i] > 0 && mtmValue[i - 1] > 0) {
            if (mtmValue[i] > mtmValue[i - 1]) {
                mtmAn.push(3)
            } else {
                mtmAn.push(4);
            }
        } else if (mtmValue[i] < 0 && mtmValue[i - 1] < 0) {
            if(mtmValue[i] > mtmValue[i - 1]) {
                mtmAn.push(5);
            } else {
                mtmAn.push(6);
            }
        } else {
            mtmAn.push(0);
        }
    }
    return mtmAn;
}

function nviAnalyse(data) {
    const NVIValue = nvi(data.close, data.volume);
    const nviMa = ma(NVIValue, 6);
    let nviAn = [0];
    for (let i = 1; i < NVIValue.length; i++) {
        if (NVIValue[i] > nviMa[i] && NVIValue[i - 1] < nviMa[i - 1]) {
            nviAn.push(1);
        } else if (NVIValue[i] < nviMa[i] && NVIValue[i - 1] > nviMa[i - 1]) {
            nviAn.push(2);
        } else if (NVIValue[i] > nviMa[i] && NVIValue[i - 1] > nviMa[i - 1]) {
            if (NVIValue[i] > NVIValue[i - 1]) {
                nviAn.push(3);
            } else {
                nviAn.push(4);
            }
        } else if (NVIValue[i] < nviMa[i] && NVIValue[i - 1] < nviMa[i - 1]) {
            if (NVIValue[i] < NVIValue[i - 1]) {
                nviAn.push(5);
            } else {
                nviAn.push(6);
            }
        } else {
            nviAn.push(0);
        }
    }
    return nviAn;
}

function obvAnalyse(data) {
    const obvValue = obv(data.close, data.volume);
    let obvAn = [0];
    for (let i = 1; i < obvValue.length; i++) {
        if (obvValue[i] > obvValue[i - 1]) {
            obvAn.push(1);
        } else if (obvValue[i] < obvValue[i - 1]) {
            obvAn.push(2);
        } else {
            obvAn.push(0);
        }
    }
    return obvAn;
}

function psyAnalyse(data) {
    const psyValue = psy(data.close, 14);
    let psyAn = [0];
    for (let i = 1; i < psyValue.length; i++) {
        if (psyValue[i] > 75 && psyValue[i - 1] < 75) {
            psyAn.push(1);
        } else if (psyValue[i] < 75 && psyValue[i - 1] > 75) {
            psyAn.push(2);
        } else if (psyValue[i] > 75) {
            if (psyValue[i] > psyValue[i - 1]) {
                psyAn.push(3);
            } else {
                psyAn.push(4);
            }
        } else if (psyValue[i] < 25 && psyValue[i - 1] > 25) {
            psyAn.push(5);
        } else if (psyValue[i] > 25 && psyValue[i - 1] < 25) {
            psyAn.push(6);
        } else if (psyValue[i] < 25 && psyValue[i - 1] < 25) {
            if (psyValue[i] > psyValue[i - 1]) {
                psyAn.push(7);
            } else {
                psyAn.push(8);
            }
        } else if (psyValue[i] > 25 && psyValue[i] < 75) {
            if (psyValue[i] > psyValue[i - 1]) {
                psyAn.push(9);
            } else {
                psyAn.push(10);
            }
        } else {
            psyAn.push(0);
        }
    }
    return psyAn;
}

function pviAnalyse(data) {
    const PVIValue = pvi(data.close, data.volume);
    const PVIMa = ma(PVIValue, 6);
    let pviAn = [0];
    for (let i = 1; i < PVIValue.length; i++){
        if (PVIValue[i] > PVIMa[i] && PVIValue[i - 1] < PVIMa[i - 1]) {
            pviAn.push(1);
        } else if (PVIValue[i] < PVIMa[i] && PVIValue[i - 1] > PVIMa[i - 1]) {
            pviAn.push(2);
        } else if (PVIValue[i] < PVIMa[i] && PVIValue[i - 1] < PVIMa[i - 1]) {
            if (PVIValue[i] > PVIValue[i - 1]) {
                pviAn.push(3)
            } else {
                pviAn.push(4);
            }

        } else if (PVIValue[i] > PVIMa[i] && PVIValue[i - 1] > PVIMa[i - 1]) {
            if (PVIValue[i] > PVIValue[i - 1]) {
                pviAn.push(5)
            } else {
                pviAn.push(6);
            }
        } else {
            pviAn.push(0);
        }
    }
    return pviAn;
}

function rocAnalyse(data) {
    const rocValue = roc(data.close, 14);
    let rocAn = [0];
    for (let i = 1; i < rocValue.length; i++) {
        if (rocValue[i] > 100 && rocValue[i - 1] < 100) {
            rocAn.push(1);
        } else if (rocValue[i] < 100 && rocValue[i - 1] > 100) {
            rocAn.push(2);
        } else if (rocValue[i] > 100) {
            if (rocValue[i] > rocValue[i - 1]) {
                rocAn.push(3);
            } else {
                rocAn.push(4);
            }
        } else if (rocValue[i] < 100) {
            if (rocValue[i] > rocValue[i - 1]) {
                rocAn.push(5);
            } else {
                rocAn.push(6);
            }
        } else {
            rocAn.push(0);
        }
    }
    return rocAn;
}

function rsiAnalyse(data) {
    const rsi6Value = rsi(data.close, 6);
    const rsi12Value = rsi(data.close, 6);
    let rsiAn = [0];
    for (let i = 1; i < rsi6Value.length; i++) {
        if (rsi6Value[i] > rsi12Value[i] && rsi6Value[i - 1] < rsi12Value[i - 1]) {
            rsiAn.push(1);
        } else if (rsi6Value[i] < rsi12Value[i] && rsi6Value[i - 1] > rsi12Value[i - 1]) {
            rsiAn.push(2);
        } else if (rsi6Value[i] > 50 && rsi6Value[i - 1] < 50) {
            rsiAn.push(3);
        } else if (rsi6Value[i] < 50 && rsi6Value[i - 1] > 50) {
            rsiAn.push(4);
        } else if (rsi6Value[i] > 80 && rsi6Value[i - 1] < 80) {
            rsiAn.push(5);
        } else if (rsi6Value[i] < 80 && rsi6Value[i - 1] > 80) {
            rsiAn.push(6);
        } else if (rsi6Value[i] > 20 && rsi6Value[i] < 20) {
            rsiAn.push(7);
        } else if (rsi6Value[i] < 20 && rsi6Value[i] > 20) {
            rsiAn.push(8);
        } else if (rsi6Value[i] > 20 && rsi6Value[i] < 80) {
            if (rsi6Value[i] > rsi6Value[i - 1]) {
                rsiAn.push(9);
            } else {
                rsiAn.push(10);
            }
        } else {
            rsiAn.push(11);
        }
    }
    return rsiAn;
}

function rviAnalyse(data) {
    const rviValue = rvi(data.close, 5, 12);
    let rviAn = [0];
    for (let i = 1; i < rviValue.length; i++) {
        if (rviValue[i] > 50 && rviValue[i - 1] < 50) {
            rviAn.push(1);
        } else if (rviValue[i] < 50 && rviValue[i - 1] > 50) {
            rviAn.push(2);
        } else if (rviValue[i] > 50) {
            if (rviValue[i] > rviValue[i - 1]) {
                rviAn.push(3);
            } else {
                rviAn.push(4);
            }
        } else if (rviValue[i] < 50) {
            if (rviValue[i] > rviValue[i - 1]) {
                rviAn.push(5);
            } else {
                rviAn.push(6);
            }
        } else {
            rviAn.push(0);
        }
    }
    return rviAn;
}

function sarAnalyse(data) {
    const { SARofCurBar, SARofNextBar, Position, Transition } = sar(data.high, data.low, 0.02, 0.2);
    let sar1An = [0];
    let sar2An = [0];
    for (let i = 1; i < Position.length; i++) {
        if (Position[i] === 1) {
            sar1An.push(1);
        } else {
            sar1An.push(2);
        }

        if (Transition[i] === 1) {
            sar2An.push(1);
        } else if (Transition[i] === -1) {
            sar2An.push(2);
        } else {
            sar2An.push(0);
        }
    }
    return { sar1An, sar2An };
}

function trixAnalyse(data) {
    const { trixValue, matrixValue } = trix(data.close, 12, 6);
    let trixAn = [0];
    for (let i = 0; i < trixValue.length; i++) {
        if (trixValue[i] > matrixValue[i] && trixValue[i - 1] < matrixValue[i - 1]) {
            trixAn.push(1);
        } else if (trixValue[i] < matrixValue[i] && trixValue[i - 1] > matrixValue[i - 1]) {
            trixAn.push(2);
        } else if (trixValue[i] > matrixValue[i] && trixValue[i - 1] > matrixValue[i - 1]) {
            if (trixValue[i] > trixValue[i - 1]) {
                trixAn.push(3);
            } else {
                trixAn.push(4);
            }
        } else if (trixValue[i] < matrixValue[i] && trixValue[i - 1] < matrixValue[i - 1]) {
            if (trixValue[i] > trixValue[i - 1]) {
                trixAn.push(5);
            } else {
                trixAn.push(6);
            }
        } else {
            trixAn.push(0);
        }
    }
    return trixAn;
}

function vrAnalyse(data) {
    const vrValue = vr(data.close, data.volume, 14);
    let vrAn = [0];
    for (let i = 1; i < vrValue.length; i++) {
        if (vrValue[i] < 70 && vrValue[i] > 40) {
            vrAn.push(1);
        } else if (vrValue[i] > 80 && vrValue[i] < 150) {
            vrAn.push(2);
        } else if (vrValue[i] > 160 && vrValue[i] < 450) {
            vrAn.push(3);
        } else if (vrValue[i] > 450) {
            vrAn.push(4);
        } else {
            vrAn.push(0);
        }
    }
    return vrAn;
}

function wadAnalyse(data) {
    const wadValue = wad(data.high, data.low, data.close);
    const wadMa = ma(wadValue, 6);
    let wadAn = [0];
    for (let i = 1; i < wadValue.length; i++) {
        if (wadValue[i] > wadMa[i] && wadValue[i - 1] < wadMa[i - 1]) {
            wadAn.push(1);
        } else if (wadValue[i] < wadMa[i] && wadValue[i - 1] > wadMa[i - 1]) {
            wadAn.push(2);
        } else if (wadValue[i] > wadMa[i] && wadValue[i - 1] > wadMa[i - 1]) {
            if (wadValue[i] > wadValue[i - 1]) {
                wadAn.push(3);
            } else {
                wadAn.push(4);
            }
        } else if (wadValue[i] < wadMa[i] && wadValue[i - 1] < wadMa[i - 1]) {
            if (wadValue[i] > wadValue[i - 1]) {
                wadAn.push(5);
            } else {
                wadAn.push(6);
            }
        } else {
            wadAn.push(0);
        }
    }
    return wadAn;
}

function wmsAnalyse(data) {
    const wmsValue = wms(data.high, data.low, data.close, 12);
    let wmsAn = [0];
    for (let i = 1; i < wmsValue.length; i++) {
        if (wmsValue[i] > 80 && wmsValue[i - 1] < 80) {
            wmsAn.push(1);
        } else if (wmsAn[i] < 80 && wmsValue[i - 1] > 80) {
            wmsAn.push(2);
        } else if (wmsValue[i] > 20 && wmsValue[i - 1] < 20) {
            wmsAn.push(3);
        } else if (wmsValue[i] < 20 && wmsValue[i - 1] > 20) {
            wmsAn.push(4);
        } else if (wmsValue[i] > 20 && wmsValue[i - 1] < 80){
            if (wmsValue[i] > wmsValue[i - 1]) {
                wmsAn.push(5);
            } else {
                wmsAn.push(6)
            }
        } else {
            wmsAn.push(0);
        }
    }
    return wmsAn;
}

function wvadAnalyse(data) {
    const wvadValue = wvad(data.open, data.high, data.low, data.close, data.volume, 14);
    let wvadAn = [0];
    for (let i = 1; i < wvadValue.length; i++) {
        if (wvadValue[i] > 0 && wvadValue[i - 1] < 0) {
            wvadAn.push(1);
        } else if (wvadValue[i] < 0 && wvadValue[i - 1] > 0) {
            wvadAn.push(2);
        } else if (wvadValue[i] > 0) {
            if (wvadValue[i] > wvadValue[i - 1]) {
                wvadAn.push(3);
            } else {
                wvadAn.push(4);
            }
        } else if (wvadAn[i] < 0) {
            if (wvadValue[i] > wvadValue[i - 1]) {
                wvadAn.push(5);
            } else {
                wvadAn.push(6);
            }
        } else {
            wvadAn.push(0);
        }
    }
    return wvadAn;
}

export default function analyse(data) {
    let { adAn, adM6An } = adAnalyse(data);
    let { arAn, brAn } = arbrAnalyse(data);
    let { upAn, downAn } = aroonAnalyse(data);
    let biasAn = biasAnalyse(data);
    let cciAn = cciAnalyse(data);
    let cmoAn = cmoAnalyse(data);
    let crAn = crAnalyse(data);
    let dmaAn = dmaAnalyse(data);
    let { dmiAn1, dmiAn2 } = dmiAnalyse(data);
    let { dpoAn, dpoMaAn } = dpoAnalyse(data);
    let { emvAn, emvMaAn } = emvAnalyse(data);
    let { ema2An, ema3An } = forceIndexAnalyse(data);
    let { kdj1, kdj2 } = kdjAnalyse(data);
    let macdAn = macdAnalyse(data);
    let mfiAn = mfiAnalyse(data);
    let mtmAn = mtmAnalyse(data);
    let nviAn = nviAnalyse(data);
    let obvAn = obvAnalyse(data);
    let psyAn = psyAnalyse(data);
    let pviAn = pviAnalyse(data);
    let rocAn = rocAnalyse(data);
    let rsiAn = rsiAnalyse(data);
    let rviAn = rviAnalyse(data);
    let { sar1An, sar2An } = sarAnalyse(data);
    let trixAn = trixAnalyse(data);
    let vrAn = vrAnalyse(data);
    let wadAn = wadAnalyse(data);
    let wmsAn = wmsAnalyse(data);
    let wvadAn = wvadAnalyse(data);
    let factors = []
    for (let i = 0; i < adAn.length; i++) {
        factors.push(
            [
                adAn[i], adM6An[i], arAn[i], brAn[i], upAn[i], downAn[i], biasAn[i],
                cciAn[i], cmoAn[i], crAn[i], dmaAn[i], dmiAn1[i], dmiAn2[i], dpoAn[i],
                dpoMaAn[i], emvAn[i], emvMaAn[i], ema2An[i], ema3An[i], kdj1[i], kdj2[i],
                macdAn[i], mfiAn[i], mtmAn[i], nviAn[i], obvAn[i], psyAn[i], pviAn[i], rocAn[i],
                rsiAn[i], rviAn[i], sar1An[i], sar2An[i], trixAn[i], vrAn[i], wadAn[i], wmsAn[i],
                wvadAn[i]
            ]
        );
    }
    return factors;
}

export function analyse2(data) {
    //let { adAn, adM6An } = adAnalyse(data);
    let { arAn, brAn } = arbrAnalyse(data);
    let { upAn, downAn } = aroonAnalyse(data);
    let biasAn = biasAnalyse(data);
    let cciAn = cciAnalyse(data);
    let cmoAn = cmoAnalyse(data);
    let crAn = crAnalyse(data);
    let dmaAn = dmaAnalyse(data);
    let { dmiAn1, dmiAn2 } = dmiAnalyse(data);
    let { dpoAn, dpoMaAn } = dpoAnalyse(data);
    //let { emvAn, emvMaAn } = emvAnalyse(data);
    //let { ema2An, ema3An } = forceIndexAnalyse(data);
    let { kdj1, kdj2 } = kdjAnalyse(data);
    let macdAn = macdAnalyse(data);
    //let mfiAn = mfiAnalyse(data);
    let mtmAn = mtmAnalyse(data);
    //let nviAn = nviAnalyse(data);
    //let obvAn = obvAnalyse(data);
    let psyAn = psyAnalyse(data);
    //let pviAn = pviAnalyse(data);
    let rocAn = rocAnalyse(data);
    let rsiAn = rsiAnalyse(data);
    let rviAn = rviAnalyse(data);
    let { sar1An, sar2An } = sarAnalyse(data);
    let trixAn = trixAnalyse(data);
    //let vrAn = vrAnalyse(data);
    let wadAn = wadAnalyse(data);
    let wmsAn = wmsAnalyse(data);
    //let wvadAn = wvadAnalyse(data);
    let factors = []
    for (let i = 0; i < arAn.length; i++) {
        factors.push(
            [
                arAn[i], brAn[i], upAn[i], downAn[i], biasAn[i],
                cciAn[i], cmoAn[i], crAn[i], dmaAn[i], dmiAn1[i], dmiAn2[i], dpoAn[i],
                dpoMaAn[i], kdj1[i], kdj2[i],
                macdAn[i], mtmAn[i], psyAn[i], rocAn[i],
                rsiAn[i], rviAn[i], sar1An[i], sar2An[i], trixAn[i], wadAn[i], wmsAn[i]
            ]
        );
    }
    return factors;
}
