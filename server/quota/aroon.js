import $V from './$V';

export default function aroon(high, low, length = 20){
    let uparoon = $V.init(high.length, 50);
    let downaroon = $V.init(high.length, 50);
    for(let i = length; i < high.length; i++) {
        let highbar = $V.argmax($V.subv(high, i - length, i));
        uparoon[i] = (length - (length - highbar + 1)) / length * 100;
        let lowbar = $V.argmin($V.subv(low, i - length, i));
        downaroon[i] = (length - (length - lowbar + 1)) / length * 100;
    }
    return { uparoon, downaroon };
}

