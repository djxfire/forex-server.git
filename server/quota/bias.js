import $V from './$V';
import ema from './ema';

export default function bias(price, length) {
    const emaValue = ema(price, length);
    return $V.mul_constant($V.div($V.sub(price, emaValue), emaValue), 100);
}
