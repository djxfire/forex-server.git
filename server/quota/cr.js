import $V from './$V';

export default function cr(high, low, close, length) {
    let tp = [];
    for (let i = 0; i < high.length; i++) {
        tp[i] = (high[i] + low[i] + close[i]) / 3;
    }
    let crValue = $V.init(high.length, 0);
    crValue = $V.set_by_index(crValue, $V.range(length), 100);
    for(let i = length; i < high.length; i++) {
        let sum1 = 0;
        let sum2 = 0;
        for (let j = i - length + 1; j <= i; j++) {
            let tmp1 = high[j] - tp[j - 1];
            let tmp2 = tp[j - 1] - low[j];
            if (tmp1 < 0) {
                tmp1 = 0;
            }
            if (tmp2 < 0) {
                tmp2 = 0;
            }
            sum1 += tmp1;
            sum2 += tmp2;
        }
        crValue[i] =sum1 / sum2 * 100;
    }
    return crValue;
}
