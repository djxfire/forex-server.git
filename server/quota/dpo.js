import $V from './$V';
import ma from './ma'

export default function dpo(price, length) {
    let DPOValue = $V.init(price.length,0);
    let offset = Math.floor(length * 0.5 + 1);
    let MAValue = ma(price,length);
    return $V.copy_by_index(DPOValue,offset,$V.sub($V.subv(price,offset,price.length),$V.subv(MAValue,0,price.length-offset)));
}
