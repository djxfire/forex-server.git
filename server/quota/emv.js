import $V from './$V';
import ma from './ma';

export default function emv(high, low, volume, N, M) {
    let EM = [0];
    for (let i = 1; i < high.length; i++) {
        let mid = (high[i] + low[i] - high[i - 1] - low[i - 1]) / 2;
        if (high[i] - low[i] === 0) {
            EM[i] = 0;
        } else {
            let bro = volume[i] / (high[i] - low[i]);
            EM[i] = mid / bro;
        }
    }
    let EMVValue = ma(EM, N);
    let MAEMVValue = ma(EMVValue, M);
    return { EMVValue, MAEMVValue };
}
