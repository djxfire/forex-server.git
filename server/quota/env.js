import $V from './$V';
import ma from './ma';

export default function env(price, width, length) {
    let MiddleLine = ma(price,length);
    let UpperLine = $V.mul_constant(MiddleLine , (1 + width));
    let LowerLine = $V.mul_constant(MiddleLine,(1 - width));
    return { UpperLine, MiddleLine, LowerLine };
}
