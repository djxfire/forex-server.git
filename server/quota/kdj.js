import $V from './$V';

export default function kdj(high, low, close, N, M, L, S) {
    let RSV = $V.init(high.length,0);
    let KValue = $V.init(high.length,0);
    let DValue = $V.init(high.length,0);
    RSV = $V.set_by_index(RSV,$V.arange(0,N),50);
    KValue = $V.set_by_index(KValue,$V.arange(0,N),50);
    DValue = $V.set_by_index(DValue,$V.arange(0,N),50);
    for(let i = N;i < high.length;i++){
        RSV[i] = (close[i] - $V.min($V.subv(low,i-N,i)))/($V.max($V.subv(high,i-N,i)) - $V.min($V.subv(low,i-N,i)))*100;
        KValue[i] = (M - 1)/M*KValue[i-1] + 1/M*RSV[i]
        DValue[i] = (L - 1)/L*DValue[i-1] + 1/L*KValue[i];
    }
    let JValue = $V.sub($V.mul_constant(DValue,S),$V.mul_constant(KValue,(S-1)));
    return { KValue, DValue, JValue };
}
