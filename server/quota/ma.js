import $V from './$V';

export default function ma(data, length) {
  let result = [];
  for(let i = 0;i < length; i++) {
    result[i] = data[i];
  }
  for (let i = length - 1; i < data.length; i++) {
    let sum = 0;
    for (let j = i - length + 1; j <= i; j++) {
      sum += data[j];
    }
    result[i] = sum / length;
  }

  return result;
}
