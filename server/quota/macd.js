import $V from './$V';
import ma from './ma';

export default function macd(data, fastLength, slowLength, deaLength) {
  let DIF = $V.init(data.length, 0);
  let DEA = $V.init(data.length, 0);
  const FEMA = ma(data, fastLength);
  const SEMA = ma(data, slowLength);
  DIF = $V.sub(FEMA, SEMA);
  DEA = ma(DIF, deaLength);
  let BAR = $V.sub(DIF, DEA);
  return { DIF, DEA, BAR };
}
