import $V from './$V';

export default function mfi(high, low, close, volume, length) {
    let MFIValue = [];
    let DiffofTP = [];
    let MF = [];
    for (let i = 0; i < high.length; i++) {
        MF[i] = (high[i] + low[i] + close[i]) / 3;
        if (i === 0) {
            DiffofTP[i] = 0;
        } else {
            DiffofTP[i] = (high[i] + low[i] + close[i] - high[i - 1] - low[i - 1] - close[i - 1]) / 3;
        }
    }
    for(let i = 0; i < high.length;i++){
        if (i < length) {
            MFIValue[i] = 50;
        } else {
            let sum1 = 0, sum2 = 0;
            for (let j = i - length; j <= i; j++) {
                sum1 += MF[j];
                if (DiffofTP[j] > 0) {
                    sum2 += MF[j];
                }
            }
            MFIValue[i] = sum2 / sum1 * 100;
        }
    }
    return MFIValue;
}
