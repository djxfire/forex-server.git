import $V from './$V';

export default function mike(high, low, close, length) {
    let TP = $V.div_constant($V.add($V.add(high,low),close),3);
    let WR = $V.init(high.length,0);
    let MR = $V.init(high.length,0);
    let SR = $V.init(high.length,0);
    let WS = $V.init(high.length,0);
    let MS = $V.init(high.length,0);
    let SS = $V.init(high.length,0);
    WR = $V.copy_by_range(WR,$V.range(length),TP);
    MR = $V.copy_by_range(MR,$V.range(length),TP);
    SR = $V.copy_by_range(SR,$V.range(length),TP);
    WS = $V.copy_by_range(WS,$V.range(length),TP);
    MS = $V.copy_by_range(MS,$V.range(length),TP);
    SS = $V.copy_by_range(SS,$V.range(length),TP);
    for(let i = length;i < high.length;i++){
        let highest = $V.max($V.subv(high,i-length,i));
        let lowest = $V.min($V.subv(low,i-length,i));
        WR[i] = TP[i] + (TP[i] - lowest);
        MR[i] = TP[i] + (highest - lowest);
        SR[i] = 2 * highest - lowest;
        WS[i] = TP[i] - (highest - TP[i]);
        MS[i] = TP[i] - (highest - lowest);
        SS[i] = 2 * lowest - highest;
    }
    return { WR, MR, SR, WS, MS, SS };
}
