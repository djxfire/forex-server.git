import $V from './$V';

export default function mtm(price, length) {
    let MTMValue = [];
    for (let i = 0; i < price.length; i++) {
        if (i < length) {
            MTMValue.push(0);
        } else {
            MTMValue[i] = price[i] - price[i - length];
        }
    }
    return MTMValue;
}
