import $V from './$V';

export default function pvi(close, volume) {
    let PVIValue = $V.init(close.length,0);
    for(var i = 0;i < close.length;i++){
        if( i == 0){
            PVIValue[i] = 100;
        }else{
            if(volume[i] > volume[i-1]){
                PVIValue[i] = PVIValue[i-1] + (close[i] - close[i-1])/close[i-1] * PVIValue[i-1];
            }else{
                PVIValue[i] = PVIValue[i-1];
            }
        }
    }
    return PVIValue;
}
