export default function roc(price, length) {
    let ROCValue = [];
    for (let i = 0; i < price.length; i++) {
        if (i < length) {
            ROCValue[i] = 0;
        } else {
            ROCValue[i] = (price[i] - price[i - length]) / price[i - length] * 100;
        }
    }
    return ROCValue;
}
