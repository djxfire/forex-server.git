import $V from './$V';

export default function rsi(price, length) {
    let rsiValue = $V.init(price.length,50);
    let diffOfPrice = $V.init(price.length,0);
    diffOfPrice = $V.copy_by_range(diffOfPrice,$V.arange(1,price.length),$V.sub($V.subv(price,1,price.length),$V.subv(price,0,price.length-1)));
    for(let i = length;i < price.length;i++){
        let sum = 0;
        let all = 0;
        for (let j = i - length; j < i; j++) {
            if(diffOfPrice[j] > 0) {
                sum += diffOfPrice[j];
            }
            all += Math.abs(diffOfPrice[j]);
        }
        rsiValue[i] = sum / all * 100;
    }
    console.timeEnd('1111');
    return rsiValue;
}
