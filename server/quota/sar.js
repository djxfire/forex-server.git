import $V from './$V';

/**
 *
 * @param afStep 初始加速因子
 * @param afLimit 加速因子最大值
 * SARofCurBar-当前Bar的停损值
 * SARofNextBar-下一个Bar的停损值
 * Position-输出建议的持仓状态，1-多头，-1-空头
 * Transition-当前Bar的状态是否发生反转，1或-1为反转，0为保持不变
 */
export default function sar(high, low, afStep, afLimit) {
    let Af = $V.init(high.length , 0);
    let SARofCurBar = $V.init(high.length, 0);
    let SARofNextBar = $V.init(high.length, 0);
    let Position = $V.init(high.length, 0);
    let Transition = 0;     // 输出是否发生反转，1表示反转多头， -1表示反转为空头，0为保持不变
    let HighestPoint = $V.init(high.length , 0);
    let LowestPoint = $V.init(high.length, 0);
    for (let i = 0; i < high.length; i++) {
        if (i === 0) {
            Position[i] = 1;
            Transition = 1;
            Af[i] = afStep;
            HighestPoint[i] = high[i];
            LowestPoint[i] = low[i];
            SARofCurBar[i] = LowestPoint[i];
            SARofNextBar[i] = SARofCurBar[i] + Af[i] * (HighestPoint[i] - SARofCurBar[i]);
            if (SARofNextBar[i] > low[i]) {
                SARofNextBar[i] = low[i];
            }
        }
        if (i > 0) {
            Transition = 0;
            HighestPoint[i] = high[i] > HighestPoint[i - 1] ? high[i] : HighestPoint[i - 1];
            LowestPoint[i] = low[i] > LowestPoint[i - 1] ? LowestPoint[i - 1] : low[i];
            // 多头情形下
            if (Position[i - 1] === 1) {
                // 如果满足反转条件
                if (low[i] <= SARofNextBar[i - 1]) {
                    Position[i] = -1;
                    Transition = -1;
                    SARofCurBar[i] = HighestPoint[i];
                    HighestPoint[i] = high[i];
                    LowestPoint[i] = low[i];
                    Af[i] = afStep;
                    SARofNextBar[i] = SARofCurBar[i] + Af[i] * (LowestPoint[i] - SARofCurBar[i]);
                    SARofNextBar[i] = $V.max([SARofNextBar[i], high[i], high[i - 1]]);
                } else {
                    Position[i] = Position[i - 1];
                    SARofCurBar[i] = SARofNextBar[i - 1];
                    if (HighestPoint[i] > HighestPoint[i - 1] && Af[i - 1] < afLimit) {
                        if (Af[i - 1] + afStep > afLimit) {
                            Af[i] = afLimit;
                        } else {
                            Af[i] = Af[i - 1] + afStep;
                        }
                    } else {
                        Af[i] = Af[i - 1];
                    }
                    SARofNextBar[i] = SARofCurBar[i] + Af[i] * (HighestPoint[i] - SARofCurBar[i]);
                    SARofNextBar[i] = $V.min([SARofNextBar[i], low[i], low[i - 1]]);
                }
            }
            // 空头情况
            if (Position[i - 1] === -1) {
                if (high[i] >= SARofNextBar[i - 1]) {
                    Position[i] = 1;
                    Transition = 1;
                    SARofCurBar[i] = LowestPoint[i];
                    HighestPoint[i] = high[i];
                    LowestPoint[i] = low[i];
                    Af[i] = afStep;
                    SARofNextBar[i] = SARofCurBar[i] + Af[i] * (HighestPoint[i] - SARofCurBar[i]);
                    SARofNextBar[i] = $V.min([SARofNextBar[i], low[i], low[i - 1]]);
                } else {
                    Position[i] = Position[i - 1];
                    SARofCurBar[i] = SARofNextBar[i - 1];
                    if (LowestPoint[i] < LowestPoint[i - 1] && Af[i - 1] < afLimit) {
                        if (Af[i - 1] + afStep > afLimit) {
                            Af[i] = afLimit;
                        } else {
                            Af[i] = Af[i - 1] + afStep;
                        }
                    } else {
                        Af[i] = Af[i - 1];
                    }
                    SARofNextBar[i] = SARofCurBar[i] + Af[i] * (LowestPoint[i] - SARofCurBar[i]);
                    SARofNextBar[i] = $V.max([SARofNextBar[i], high[i], high[i - 1]]);
                }
            }
        }
    }
    return { SARofCurBar, SARofNextBar, Position, Transition }

}
