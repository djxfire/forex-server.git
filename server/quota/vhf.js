export default function vhf(price, length) {
    let vhfValue = [];
    let diffOfPrice = [0];
    for (let i = 1; i < price.length; i++) {
        diffOfPrice[i] = Math.abs(price[i] - price[i - 1]);
    }
    for(let i = 0;i < price.length;i++){
        if (i < length) {
            vhfValue[i] = 0.5;
        } else {
            let max = price[i], min = price[i];
            let sum = 0;
            for (let j = i - length + 1; j <= i; j++) {
                if (max < price[j]) {
                    max = price[j]
                }
                if (min > price[j]) {
                    min = price[j];
                }
                sum += diffOfPrice[j];
            }
            vhfValue[i] = (max - min) / sum;
        }
    }
    return vhfValue;
}
