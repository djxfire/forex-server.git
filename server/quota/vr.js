import $V from './$V';

export default function vr(price, volume, length) {
    let vrValue = [];
    let diff = [0];
    for(let i = 1; i < price.length; i++) {
        diff[i] = price[i] - price[i - 1];
    }
    for(let i = 0;i < price.length;i++){
        if(i < length){
            let sum1 = 0;
            let sum2 = 0;
            let sum3 = 0;
            for (let j = 0; j <= i; j++) {
                if (diff[j] > 0) {
                    sum1 += volume[j];
                } else if (diff[j] < 0){
                    sum2 += volume[j];
                } else {
                    sum3 += volume[j];
                }
            }
            vrValue[i] = sum1 / (sum2 + 0.5 * sum3) * 100;
        }
        if(i >= length){
            let sum1 = 0;
            let sum2 = 0;
            let sum3 = 0;
            for (let j = i - length + 1; j <= i; j++) {
                if (diff[j] > 0) {
                    sum1 += volume[j];
                } else if (diff[j] < 0){
                    sum2 += volume[j];
                } else {
                    sum3 += volume[j];
                }
            }
            vrValue[i] = sum1 / (sum2 + 0.5 * sum3) * 100;
        }
    }
    return vrValue;
}
