import $V from './$V';

export default function wms(high, low, close, N) {
    let wmsValue = [];
    for(let i = 0;i< high.length;i++){
        if (i < N) {
            wmsValue[i] = 50;
        } else {
            let max = high[i];
            let min = low[i];
            for (let j = i - N + 1; j <= i; j++) {
                if (max < high[j]) {
                    max = high[j];
                }
                if (min > low[j]) {
                    min = low[j];
                }
            }
            wmsValue[i] = (max - close[i]) / (max - min) * 100;
        }
    }
    return wmsValue;
}
