/*
* @Date: 2021/2/17
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import charset from 'superagent-charset'
import superagent from 'superagent'

export function get(url, params, headers, set = "utf-8") {
  return new Promise((resolve, reject) => {
    let request = charset(superagent)
    let req = request.get(url)
      .query(params)
      .set('Accept', '*/*')
      .set('User-Agent','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.3538.400 QQBrowser/9.7.12954.400')
      .set('Accept-Language', 'zh-CN')
    for(let key in headers) {
      req.set(key, headers[key])
    }
    req.buffer(true)
      .charset(set)
      .end((err, res) => {
        if(err) {
          reject(err)
        }else {
          resolve(res)
        }
      })
  })
}

export default {
  get,
}
