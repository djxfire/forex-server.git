/*
* @Date: 2021/1/31
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
export default {
  rules : {
    name    : 'require',
  },
  messages : {
    'name.require'  : '名称不能为空',
  },
  scenes : {
    'add' : ['name'],
    'edit' : ['name']
  }
}
