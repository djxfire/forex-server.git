//数据校验错误
export const VALIDATE_ERROR = 'VALIDATE_ERROR'
export const ONLOADING = 'ONLOADING'

//LOGIN
export const LOGIN = 'LOGIN'
export const ON_LOGINING = 'ON_LOGINING'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAIL = 'LOGIN_FAIL'

//Auth
export const AUTH = 'AUTH'
export const AUTH_SUCCESS = 'AUTH_SUCCESS'
export const AUTH_FAIL = 'AUTH_FAIL'

