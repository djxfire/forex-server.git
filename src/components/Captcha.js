import React from 'react'
import './captcha.scss'
import {PropTypes} from 'prop-types'

export default class Captcha extends React.Component{
    static propTypes = {
        onValueChange:PropTypes.func
    }
    constructor(props) {
        super(props)
        this.state = {
            captcha:Math.random()
        }
        this.refresh = this.refresh.bind(this)
        this.onValueChange = this.onValueChange.bind(this)
    }
    refresh(e) {
        let captcha = Math.random()
        this.setState({
            captcha: captcha
        })
    }
    onValueChange(e) {
       let value = e.target.value
       if(this.props.onValueChange) {
           this.props.onValueChange(value)
       }
    }
    render () {
        return (
            <div className="form-group">
                <label>验证码</label>
                <div className="barcode-box">
                    <div className="barcode-input">
                        <input type="text" name="captcha" className="form-control" placeholder="请输入验证码" onChange={this.onValueChange}/>
                    </div>
                    <div className="barcode-image">
                        <img src={`/captcha?${this.state.captcha}${this.props.reset?this.props.reset:''}`} className="captcha" onClick={this.refresh}/>
                    </div>
                </div>
            </div>
        )
    }

}