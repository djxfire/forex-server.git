import React from 'react'
import {Layout,Icon,Alert,Dropdown,Menu,Modal} from 'antd'
import Nav from './Nav'
import './fragment.scss'
import PropTypes from 'prop-types'
import Mask from './Mask'
import Loading from './Loading'


class UserModal extends React.Component {
    render() {
        let {content, title, visiable, onOk, onCancel} = this.props
        if(!visiable) {
            return null
        }
        return <Modal title = {title} visible="true"
                      onOk={onOk} onCancel={onCancel}>
            <div>
                {content}
            </div>
        </Modal>
    }
}

const Fragment = (selectedKey) =>
    (WrappedComponent) =>
        class extends React.Component{
            constructor(props) {
                super(props)
                this.state = {
                    ishide:false,
                    alert_message:'',
                    alert_type:'warning',
                    alert_visiable:false,
                    modal_title: '对话框',
                    modal_visible:false,
                    modal_content:null,
                    modal_onOk:()=>{},
                    modal_onCancel:() => {},
                    loading_visiable:false,
                }
                this.onHideMenu = this.onHideMenu.bind(this)
                if(WrappedComponent.contextTypes){
                    WrappedComponent.contextTypes.alert = PropTypes.func
                    WrappedComponent.contextTypes.modal = PropTypes.func
                    WrappedComponent.contextTypes.loading = PropTypes.func
                }else {
                    WrappedComponent.contextTypes = {
                        alert: PropTypes.func,
                        modal: PropTypes.func,
                        loading: PropTypes.func,
                    }
                }
            }
            onHideMenu() {
                this.setState({
                    ishide:!this.state.ishide,
                })
            }
            static childContextTypes = {
                alert: PropTypes.func,
                modal: PropTypes.func,
                loading: PropTypes.func,
            }
            getChildContext() {
                return {
                    alert: (type, message) =>{
                        this.setState({
                            alert_type: type,
                            alert_message: message,
                            alert_visiable: true
                        })
                        setTimeout(()=> {
                            this.setState({
                                alert_visiable: false
                            })
                        },3000)
                    },
                    loading: (isVisible = false) => {
                        this.setState({
                            loading_visiable: isVisible
                        })
                    },
                    modal: (content,title, onOk, onCancel) => {

                        this.setState({
                            modal_content: content,
                            modal_onOk: async () => {
                                onOk && await onOk()
                                && this.setState({
                                    modal_visible: false
                                })
                            },
                            modal_onCancel: async () => {
                                onCancel && await onCancel()
                                this.setState({
                                    modal_visible:false
                                })
                            },
                            modal_title: title || '对话框',
                            modal_visible: true
                        })

                    }
                }
            }
            render() {
                let userMenu = (
                    <Menu>
                        <Menu.Item>
                            <a href = '/ucenter'>个人中心</a>
                        </Menu.Item>
                        <Menu.Item>
                            <a href = '/logout'>退出登陆</a>
                        </Menu.Item>
                    </Menu>
                )
               return (
                   <div class = "full-screen">
                       <Layout className="full-screen" >
                           <Layout.Sider width={this.state.ishide?0:200}>
                               <Nav selected={selectedKey} className="full-screen" />
                           </Layout.Sider>
                           <Layout>
                               <Layout.Header className="header">
                                   <div class = "flex1">
                                       <Icon type={this.state.ishide?"menu-unfold":"menu-fold"}  onClick={this.onHideMenu} style={{fontSize:'18px',color:'#999'}}/>
                                   </div>

                                   <div className = "header-info">
                                       <Dropdown overlay={userMenu}>
                                           <Icon type="user" style={{fontSize:'18px',color:'#999'}}/>
                                       </Dropdown>

                                   </div>
                               </Layout.Header>
                               <Layout.Content>
                                   <div style={{display:this.state.alert_visiable?'block':'none'}}>
                                       <Alert message={this.state.alert_message} type={this.state.alert_type} showIcon/>
                                   </div>
                                   <div className = "main">
                                       <WrappedComponent {...this.props}  />
                                   </div>
                               </Layout.Content>
                               <Layout.Footer className="copyright">
                                   杭州海蔚时尚服饰有限公司版权所有
                               </Layout.Footer>
                           </Layout>
                       </Layout>
                       <UserModal onOk = {this.state.modal_onOk} onCancel={this.state.modal_onCancel} title={this.state.modal_title} visiable = {this.state.modal_visible} content = {this.state.modal_content}/>
                       <Mask isShow={this.state.loading_visiable}>
                           <Loading isShow={this.state.loading_visiable}/>
                       </Mask>
                   </div>
               )
            }
        }

export default Fragment