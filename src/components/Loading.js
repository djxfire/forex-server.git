import React from 'react'
import  './loading.scss'
import {PropTypes} from 'prop-types'

export default class Loading extends React.Component {
    constructor(props) {
        super(props)
    }
    defaultProps = {
        isShow: PropTypes.bool.isRequired
    }
    render() {
        return (
            <div className="loading" style={{display:this.props.isShow?'block':'none'}}>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        )
    }
}