import React from 'react'
import {PropTypes} from 'prop-types'
export default class Mask extends React.Component {
    static propTypes={
        isShow: PropTypes.bool.isRequired,
        onOverClicked: PropTypes.func
    }
    static defaultProps = {
        isShow: false
    }
    render() {
        return (
            <div style={{display:this.props.isShow?'block':'none'}}>
                <div className="overlay" onClick={this.props.onOverClicked}>

                </div>
                <div className="overpop">
                    {this.props.children}
                </div>
            </div>

        )
    }

}