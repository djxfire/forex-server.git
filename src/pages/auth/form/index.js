/*
* @Date: 2021/1/31
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from '_react@17.0.1@react';
import { Form, Input, Switch, Tree, TreeSelect } from 'antd';
import { FormReceiver } from "../../../utils/FormReceiver";
const formItemLayout = {
  labelCol: {span: 4},
  wrapperCol: {span: 16}
}
const TreeNode = Tree.TreeNode;

function getTreeNodes(menus) {
  return menus.map(vo => {
    if (vo.childs.length === 0 && vo.buttons.length === 0) {
      return <TreeNode title={vo.name} key={vo.id} value={vo.id} />
    }
    return (
      <TreeNode
        title={vo.name}
        key={vo.id}
        value={vo.id}
      >
        { getTreeNodes([...vo.childs, ...vo.buttons ]) }
      </TreeNode>
    )
  })
}

export default class extends React.Component {
  onChangeTree = (value) => {
    console.log(value);
    const { onAuthChange } = this.props;
    onAuthChange && onAuthChange(value.map(vo => vo.value));
  }

  render() {
    const { record, nodes = [], checkeds = [] } = this.props;
    return (
      <Form>
        <Form.Item
          label="名称"
          { ...formItemLayout }
        >
          <Input onChange={ FormReceiver('name',record )} defaultValue={ record.name } />
        </Form.Item>
        <Form.Item
          label="状态"
          { ...formItemLayout }
        >
          <Switch
            checkedChildren="启用"
            unCheckedChildren="禁用"
            onChange={ FormReceiver( 'status', record,{
              callback: value => value ? 1 : 2
            })}
            defaultChecked={Number(record.status) === 1}
          />
        </Form.Item>
        <Form.Item
          label="菜单权限"
          { ...formItemLayout }
        >
          <TreeSelect
            treeCheckable
            showSearch
            multiple
            treeCheckStrictly
            onChange={this.onChangeTree}
            defaultValue={checkeds.map(vo => ({ value: vo }))}
            showCheckedStrategy={TreeSelect.SHOW_ALL}
          >
            { getTreeNodes(nodes) }
          </TreeSelect>
        </Form.Item>
        <Form.Item
          label="备注"
          { ...formItemLayout }
        >
          <Input.TextArea
            rows={4}
            defaultValue={record.remark}
            onChange={FormReceiver('remark', record)}
          />
        </Form.Item>
      </Form>
    )
  }
}
