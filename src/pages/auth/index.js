/*
* @Date: 2021/1/31
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import '../../scss/app.scss';
import Fragment from '../../components/Fragment';
import RemoteTable from '../../components/RemoteTable';
import { Table, Icon, Switch } from 'antd';
import { authOptAction } from '../../actions/auth';
import { authReducer } from '../../reducers/auth';
import ReactDOM from "react-dom";
import connect from 'react-redux/es/connect/connect';
import Provider from 'react-redux/es/components/Provider';
import {createStore,applyMiddleware} from 'redux';
import fetchMiddleware from '../../middleware/fetchMiddleware';
import Form from './form';
import thunk from 'redux-thunk'
import { post, get } from '../../utils/http';
import AuthValidator from '../../validator/auth'
import Validator from '../../validator/index'
import { message, Button } from 'antd';

@Fragment('sys/auth')
class App extends React.Component {
  onEdit = async (record) => {
    const form = { ...record };
    const data = await get('/api/sorted');
    const checkResult = await get('/auth/getChecked', { id: `${record.id}` });
    const nodes = data.data.menus;
    let checkeds = checkResult.data.ids;
    this.context.modal(
      <Form
        record={form}
        nodes={nodes}
        checkeds={[...checkeds]}
        onAuthChange={ values => checkeds = values }
      />,
      '编辑权限',
      async () => {
        let validator = new Validator(AuthValidator)
        let res = await validator.check(form)
        if(res !== true) {
          message.error(res)
          return false
        }
        try{
          let data = await post('/auth/edit', { ...form, authRecord: checkeds })
          message.info(data.msg)
          this.refs.authTable.refresh()
          return true
        }catch(err){
          if(Number(err.type) === 1) {
            message.error(err.error)
            return false
          }
        }
      }
    );
  }

  onDelete = async  (record) => {
    try{
      let data = await post('/auth/del', { id:record.id })
      message.info(data.msg)
      this.refs.authTable.refresh()
    }catch(err) {
      if(err.type === 1) {
        message.error(err.error)
      }
    }
  }

  onAdd = async () => {
    const form = {};
    let data = await get('/api/sorted');
    const nodes = data.data.menus;
    let checkeds = [];
    this.context.modal(
      <Form record={form} nodes={nodes} checkeds={checkeds} onAuthChange={(value) => checkeds = value}/>,
      '添加权限',
      async () => {
        let validator = new Validator(AuthValidator)
        let res = await validator.check(form)
        if(res !== true) {
          message.error(res)
          return false
        }
        try{
          let data = await post('/auth/add', { ...form, authRecord: checkeds })
          message.info(data.msg)
          this.refs.authTable.refresh()
          return true
        }catch(err){
          if(Number(err.type) === 1) {
            message.error(err.error)
            return false
          }
        }
      }
    );
  }

  onChecked = async (checked, record) => {
    try{
      let data = await post('/auth/status', { id: record.id, status: checked ? 1 : 2 })
      message.info(data.msg);
      console.log('auth table', this.refs.authTable)
      this.refs.authTable.refresh()
    }catch(err) {
      if(err.type === 1) {
        message.error(err.error)
      }
      console.error(err)
    }
  }

  render() {
    return (
      <div className="full-screen">
        <div className="app-operation">
          <Button onClick={this.onAdd}>新增</Button>
        </div>
        <RemoteTable
          url="/auth/list"
          ref="authTable"
          dataProxy={(result) => {
            if (Number(result.code) === 0 && result.data && result.data.records) {
              const { data = [], cur, pageSize, total } = result.data.records;
              return {
                data,
                pagination: {
                  current: cur,
                  pageSize,
                  total,
                }
              }
            }
            return {
              data: [],
              pagination: {}
            };
          }}
        >
          <Table.Column
            title="名称"
            dataIndex="name"
            key="name"
          />
          <Table.Column
            title="备注"
            dataIndex="remark"
            key="remark"
          />
          <Table.Column
            title="状态"
            dataIndex="status"
            key="status"
            render={(text, record) => (
              <Switch
                checkedChildren="启用"
                unCheckedChildren="禁用"
                checked={Number(record.status) === 1}
                onChange={(checked) => this.onChecked(checked, record)}
              />
            )}
          />
          <Table.Column
            title="操作"
            dataIndex="action"
            key="action"
            render={(text, record) => (
              <span>
                <Icon type="edit" onClick={() => {this.onEdit(record)}}/>
                <Icon type="close" onClick={() => {this.onDelete(record)}}/>
            </span>
            )}
          />
        </RemoteTable>
      </div>
    );
  }
}

const Page = connect(
  state => ({ isloading:state.isloading, errMsg: state.errMsg }),
  { authOptAction }
)(App)

let store = createStore(authReducer, applyMiddleware(fetchMiddleware, thunk));
ReactDOM.render(
  <Provider store={store}><Page/></Provider>,
  document.getElementById('root')
)
