import './login.scss'
import '../../scss/app.scss'
import React from 'react'
import Captcha from '../../components/Captcha'
import Loading from '../../components/Loading'
import Mask from '../../components/Mask'
import configStore from '../../store'
import {Provider, connect} from 'react-redux'
import {loginReducer} from '../../reducers/login'
import {loginAction} from '../../actions/login'
import ReactDOM from 'react-dom'


class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            captcha: ''
        }
        this.onUserNameChange = this.onUserNameChange.bind(this)
        this.onPasswordChange = this.onPasswordChange.bind(this)
        this.onCaptchaChange = this.onCaptchaChange.bind(this)
        this.onLogin = this.onLogin.bind(this)
    }
    onUserNameChange(e) {
        this.setState({
            username: e.target.value
        })
    }
    onPasswordChange(e) {
        this.setState({
            password: e.target.value
        })
    }
    onCaptchaChange(val) {
        this.setState({
            captcha: val
        })
    }
    onLogin(e) {
        this.props.loginAction(this.state)
    }

    render() {
        let {isloading,errMsg} = this.props
        return (
            <div className="page">
                <Mask isShow={isloading}>
                    <Loading isShow={isloading}/>
                </Mask>

                <div className = "login-panel">
                    <div className = "login-box">
                        <div className="login-title"><h3>登录</h3></div>
                        <div className = "errMsg"
                            style={{display: errMsg !== undefined && errMsg !== '' ? 'block' : 'none'}}>{errMsg}</div>
                        <div className="form-group">
                            <label>用户名</label>
                            <input type="text" className="form-control" placeholder="请输入用户名或者邮箱"
                                   onChange={this.onUserNameChange}/>
                        </div>
                        <div className="form-group">
                            <label>密码</label>
                            <input type="password" className="form-control" placeholder="请输入密码"
                                   onChange={this.onPasswordChange}/>
                        </div>
                        <Captcha onValueChange={this.onCaptchaChange} reset = {this.props.captcha}/>
                        <div className="form-group">
                            <button className="btn btn-primary btn-block" onClick={this.onLogin}>登录</button>
                        </div>
                        <div className="form-group row">
                            <div className="col"><a href="#">忘记密码</a></div>
                        </div>
                    </div>

                </div>
            </div>

        )
    }
}

const Page = connect(
    state => ({isloading:state.isloading,errMsg: state.errMsg,captcha:state.captcha}),
    {loginAction}
)(App)

let store = configStore(loginReducer)
ReactDOM.render(
    <Provider store={store}><Page/></Provider>,
    document.getElementById('root')
)
