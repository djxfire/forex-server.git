/*
* @Date: 2021/2/10
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import { Form, Input, Switch, Select } from 'antd';
import { FormReceiver } from "../../../utils/FormReceiver";
const formItemLayout = {
  labelCol: {span: 4},
  wrapperCol: {span: 16}
}

export default class extends React.Component {
  render() {
    const { record } = this.props;
    return (
      <Form>
        <Form.Item
          label="合约名称"
          { ...formItemLayout }
        >
          <Input onChange={ FormReceiver('name',record )} defaultValue={ record.name } />
        </Form.Item>
        <Form.Item
          label="合约代码"
          { ...formItemLayout }
        >
          <Input onChange={ FormReceiver('symbol',record )} defaultValue={ record.symbol } />
        </Form.Item>
        <Form.Item
          label="产品代码"
          { ...formItemLayout }
        >
          <Input onChange={ FormReceiver('secid',record )} defaultValue={ record.secid } />
        </Form.Item>
        <Form.Item
          label="行情图表"
          { ...formItemLayout }
        >
          <Select
            allowClear
            mode="multiple"
            onChange={(e) => record['lineType'] = e.join(',')}
            defaultValue={record.lineType ? record.lineType.split(',') : []}
          >
            <Select.Option value="1m">分时</Select.Option>
            <Select.Option value="5d">五日</Select.Option>
            <Select.Option value="5m">5分</Select.Option>
            <Select.Option value="15m">15分</Select.Option>
            <Select.Option value="30m">30分</Select.Option>
            <Select.Option value="60m">60分</Select.Option>
            <Select.Option value="4h">4小时</Select.Option>
            <Select.Option value="1d">日</Select.Option>
            <Select.Option value="1w">周</Select.Option>
            <Select.Option value="1M">月</Select.Option>
            <Select.Option value="1Y">年</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="文根"
          { ...formItemLayout }
        >
          <Input onChange={ FormReceiver('prefix',record )} defaultValue={ record.prefix } />
        </Form.Item>
        <Form.Item
          label="URL"
          { ...formItemLayout }
        >
          <Input onChange={ FormReceiver('url',record )} defaultValue={ record.url } />
        </Form.Item>
        <Form.Item
          label="状态"
          { ...formItemLayout }
        >
          <Switch
            checkedChildren="启用"
            unCheckedChildren="禁用"
            onChange={ FormReceiver( 'status', record,{
              callback: value => value ? 1 : 2
            })}
            defaultChecked={Number(record.status) === 1}
          />
        </Form.Item>
        <Form.Item
          label="有效位数"
          { ...formItemLayout }
        >
          <Input onChange={ FormReceiver('decimal',record )} defaultValue={ record.decimal } />
        </Form.Item>
      </Form>
    )
  }
}
