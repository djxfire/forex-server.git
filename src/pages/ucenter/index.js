import React from 'react'

import './ucenter.scss'
import ReactDOM from 'react-dom'

import Fragment from '../../components/Fragment'
import {Button} from 'antd'
import PropTypes from 'prop-types'


@Fragment('/index')
class App extends React.Component{

    constructor(props) {
        super(props)
    }
    render() {
        return (
            <Button onClick={()=>{this.context.alert('warning','警告测试')}}>提醒</Button>
        )
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
)