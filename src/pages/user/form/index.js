/*
* @Date: 2021/2/1
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import React from 'react';
import { Form, Input, Switch, Select } from 'antd';
import { FormReceiver } from "../../../utils/FormReceiver";
const formItemLayout = {
  labelCol: {span: 4},
  wrapperCol: {span: 16}
}
export default class extends React.Component {
  onAuthChange = (value) => {
    const { onAuthChange } = this.props;
    onAuthChange && onAuthChange(value);
  }

  render() {
    const { record = {}, nodes = [], selecteds = [] } = this.props;
    return (
      <Form>
        <Form.Item
          label="用户名"
          {...formItemLayout}
        >
          <Input onChange={FormReceiver('username', record)} defaultValue={ record.username }/>
        </Form.Item>
        <Form.Item
          label="邮箱"
          {...formItemLayout}
        >
          <Input type="email" onChange={FormReceiver('email', record)} defaultValue={ record.email} />
        </Form.Item>
        <Form.Item
          label="密码"
          {...formItemLayout}
        >
          <Input type="password" onChange={FormReceiver('password', record)} />
        </Form.Item>
        <Form.Item
          label="状态"
          {...formItemLayout}
        >
          <Switch
            checkedChildren="启用"
            unCheckedChildren="禁用"
            onChange={ FormReceiver( 'valid', record,{
              callback: value => value ? 1 : 2
            })}
            defaultChecked={Number(record.valid) === 1}
          />
        </Form.Item>
        <Form.Item
          label="权限"
          {...formItemLayout}
        >
          <Select
            mode="multiple"
            style={{ width: '100%' }}
            placeholder="请选择"
            onChange={this.onAuthChange}
          >
            {
              nodes.map(vo => (
                <Select.Option value={vo.id} key={vo.id}>{vo.name}</Select.Option>
              ))
            }
          </Select>
        </Form.Item>
      </Form>
    );
  }
}
