import { AUTH, AUTH_SUCCESS, AUTH_FAIL, ONLOADING } from '../actionTypes'

export function authReducer(state, action) {
    switch (action.type) {
        case AUTH:
            return {
                isloading: true
            }
        case ONLOADING:
            return {
                isloading: true
            }
        case AUTH_SUCCESS:
            return {
                isloading: false,
                buttons: action.json
            }
        case AUTH_FAIL:
            return {
                isloading: false,
                errMsg: action.error
            }
        default:
            return {
                isloading: true
            }
            break

    }
}

