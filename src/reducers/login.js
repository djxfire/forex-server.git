import {LOGIN,ON_LOGINING,LOGIN_SUCCESS,LOGIN_FAIL, VALIDATE_ERROR} from '../actionTypes'

export function loginReducer (state, action) {
    if (action.type === LOGIN) {
        return {
            isloading:true
        }
    }else if(action.type === ON_LOGINING) {
        return {
            isloading: true,
        }
    }else if(action.type === LOGIN_SUCCESS) {
        return {
            isloading: false,
            loginInfo: action.loginInfo,
            response: action.json
        }

    }else if(action.type === LOGIN_FAIL) {
        return {
            isloading: false,
            errMsg: action.error,
            captcha: Math.random()
        }
    }else if(action.type === VALIDATE_ERROR) {
        return {
            isloading: false,
            errMsg:action.error,
            captcha: Math.random()
        }
    }else {
        return {
            isloading: false,
            errMsg:'',
            captcha:''
        }
    }
}