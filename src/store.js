import {createStore,applyMiddleware} from 'redux';
import fetchMiddleware from './middleware/fetchMiddleware';
import thunk from 'redux-thunk'

export default function configStore(reducer) {
    const store = createStore(reducer, applyMiddleware(thunk))
    return store
}
