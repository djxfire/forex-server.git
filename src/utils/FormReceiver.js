/**
 * 用于存储表单数据
 */
export function FormReceiver(key, target, options) {
    return (e) => {
        const value = e instanceof Object ? e.currentTarget.value : e;
        target[key] = (options && options.callback) ? options.callback(e) : value;
    }
}
