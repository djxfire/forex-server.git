export function randstr() {
    return Math.random().toString(36).substr(2)
}
export function isGeneratorFunction(obj) {
    let constructor = obj.constructor
    if(!constructor) return false
    if('GeneratorFunction' === constructor.name || 'GeneratorFunction' === constructor.displayName) return true
    return 'function' == typeof obj.next && 'function' == typeof obj.throw()
}

export function isResponseCode(_var ){
    if(!/^[2-5][0-9]{2}$/.test(_var)) {
        return false
    }
    return true
}
export function isArray(_var) {
    if(typeof _var === 'object' && Object.prototype.toString.call(_var) === '[object Array]') {
        return true
    }
    return false
}
export function isObject( _var ){
    if(typeof _var === 'object' && Object.prototype.toString.call(_var) === '[object Object]') {
        return true
    }
    return false
}
export function isPromise( _var ) {
    if(typeof _var === 'object' && Object.prototype.toString.call(_var) === '[object Promise]') {
        return true
    }
    return false
}
export function stamp2date  (inputTime) {
    let date = new Date(inputTime)
    let y = date.getFullYear()
    let m = date.getMonth() + 1
    m = m < 10 ? ('0' + m) : m
    let d = date.getDate()
    d = d < 10 ? ('0' + d) : d
    let h = date.getHours()
    h = h < 10 ? ('0' + h) : h
    let minute = date.getMinutes()
    let second = date.getSeconds()
    minute = minute < 10 ? ('0' + minute) : minute
    second = second < 10 ? ('0' + second) : second
    return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second
}

export function isClass(_var) {
    return typeof _var === 'function' && _var.prototype.constructor === _var
}

export function urlEncode(param, key, encode) {
    if (param==null) return '';
    var paramStr = '';
    var t = typeof (param);
    if (t == 'string' || t == 'number' || t == 'boolean') {
        paramStr += '&' + key + '='  + ((encode==null||encode) ? encodeURIComponent(param) : param);
    } else {
        for (var i in param) {
            var k = key == null ? i : key + (param instanceof Array ? '[' + i + ']' : '.' + i)
            paramStr += urlEncode(param[i], k, encode)
        }
    }
    return paramStr;

}
