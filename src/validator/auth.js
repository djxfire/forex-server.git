/*
* @Date: 2021/1/31
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
export default {
  rules : {
    name    : 'require|length:0,12',
  },
  messages : {
    'name.require'  : '名称不能为空',
    'name.length'   : '名称长度在0至12之间',
  }
}
