
let inner = {
    'require'   : check_require,
    'number'    : check_number,
    'float'     : check_float,
    'boolean'   : check_boolean,
    'email'     : check_email,
    'date'      : check_date,
    'max'       : check_max,
    'min'       : check_min,
    'tel'       : check_tel,
    'length'    : check_length
}
function check_length($val, $rule, $data) {
    let min_len = $rule.params[0] || 0
    let max_len = $rule.params[1]
    if($data[$val].length <= min_len) {
        return $rule.message
    }else if(max_len != undefined && $data[$val].length >= max_len) {
        return $rule.message
    }
    return true
}
function check_tel($val, $rule, $data) {
    if(!/^((13[0-9])|(14[5,7,9])|(15[^4])|(18[0-9])|(17[0,1,3,5,6,7,8]))\d{8}$/.test($data[$val])) {
        return $rule.message
    }
    return true
}
function check_min($val, $rule, $data) {
    if($data[$val] < $rule.params[0]) {
        return $rule.message
    }
    return true
}
function check_max($val, $rule, $data) {
    if($data[$val] > $rule.params[0]) {
        return $rule.message
    }
    return true
}
function check_date($val, $rule, $data) {
    return true
}

function check_email($val, $rule, $data) {
    if(!/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/.test($data[$val])) {
        return $rule.message
    }
    return true
}
function check_boolean($val, $rule, $data) {
    if($data[$val] != true || $data[$val] != false){
        return  $rule.message
    }
    return true

}
function check_float($val, $rule, $data) {
    if(!/^[0-9]*\.[0-9]/.test('' + $data[$val])) {
        return $rule.message
    }
    return true
}
function check_number($val, $rule, $data) {
    if(!/^[0-9]*$/.test('' + $data[$val])) {
        return $rule.message
    }
    return true
}
function check_require($val, $rule, $data) {
    if($data[$val] === undefined) {
        return  $rule.message
    }
    return true
}

export default class Validator{
    constructor(config) {
        this.config = config
    }
    scene(scene) {
        this.scen = scene
        return this
    }
    async check(_form) {
        let rules = []
        if(this.scen) {
            for(let _ of this.config.scenes[this.scen]) {
                rules.push(_)
            }
        }else {
            for(let r in this.config.rules) {
                rules.push(r)
            }
        }
        for(let __ of rules) {
            let rule = this.config.rules[__]
            let validates = rule.split('|')
            for(let v of validates) {
                let tmp = v.split(':')
                let filter = tmp[0]
                let $rule = {params:[]}
                if(tmp[1] !== undefined) {
                    let param_tmp = tmp[1].split(',')
                    for(let p of param_tmp) {
                        $rule.params.push(p)
                    }
                }
                $rule.message = this.config.messages[__ + "." + filter]
                if(inner[filter] !== undefined) {
                    let res = await inner[filter](__, $rule, _form)
                    if(res !== true) {
                        return res
                    }
                }else {
                    if(this.config[filter] !== undefined) {
                        let res = await this.config[filter](__, $rule, _form)
                        if(res !== true) {
                            return res
                        }
                    }else {
                        console.warn('校验器' + filter + '未定义')
                    }
                }
            }
        }
        return true
    }
}