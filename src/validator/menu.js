export default {
    rules : {
        name    : 'require|length:0,8',
        rule    : 'require',
        sort    : 'number',
        pid     : 'require'
    },
    messages : {
        'name.require'  : '名称不能为空',
        'name.length'   : '名称长度在0至8之间',
        'rule.require'     : '规则不能为空',
        'sort.number'       :  '排序必须为数字',
        'pid.require'  :   '上级菜单不能为空',
    }
}