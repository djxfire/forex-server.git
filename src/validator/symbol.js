/*
* @Date: 2021/2/10
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
export default {
  rules: {
    name: 'require',
    symbol: 'require',
    lineType: 'require',
    prefix: 'require',
    url: 'require',
    decimal: 'require|number'
  },
  messages: {
    'name.require': '合约名称不能为空',
    'symbol.require': '合约代码不能为空',
    'lineType.require': '行情图表不能为空',
    'prefix.require': '文根不能为空',
    'url.require': 'URL不能为空',
    'decimal.require': '有效位数不能为空',
    'decimal.number': '有效位数必须为数字',
  }
}
