/*
* @Date: 2021/2/1
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
export default {
  rules : {
    username    : 'require|length:6,32',
    email       : 'require|email',
    password    : 'require|length:6,32|mixin'
  },
  messages : {
    'username.require'  : '用户名不能为空',
    'username.length'   : '用户名长度在6至32之间',
    'email.require'     : '邮箱不能为空',
    'email.email'       :  '邮箱格式错误',
    'password.require'  :   '密码不能为空',
    'password.length'   :   '密码长度在6到32之间'
  },
  scenes : {
    'login' : ['username','password'],
    'register': ['username','email','password'],
    'edit' : ['username','email']
  },
  mixin($val, $rule, $data) {
    if(/[0-9]{1}/.test($data[$val]) && /[a-zA-Z]{1}/.test($data[$val]) && /[^0-9a-zA-Z]{1}/.test($data[$val])) {
      return true
    }
    return '密码必须至少包含数字、字母和其他字符'
  }
}
