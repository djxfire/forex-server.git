/*
* @Date: 2021/2/12
* @Author: XueChengwu <xuechengwu@erayt.com>
* @Copyright: 2015-2019 Erayt, Inc.
* @Description: If you have some questions, please contact: xuechengwu@erayt.com.
*/
import charset from 'superagent-charset'
import superagent from 'superagent';
import qs from 'qs';
let request = charset(superagent)
export default function get(url, params = {}) {
  const paramsStr = qs.stringify(params);
  return new Promise((resolve, reject) => {
    request.get(`${url}?${paramsStr}`)
      .set('Accept','*/*')
      .set('User-Agent','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.3538.400 QQBrowser/9.7.12954.400')
      .set('Accept-Language','zh-CN')
      .buffer(true)
      .charset('utf-8')
      .end((err, res) => {
        if(err) {
          reject(err)
          return
        }
        resolve(res)
      });
  })

}
