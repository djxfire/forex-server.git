const fs = require('fs')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')

module.exports = (dir) => {
    let ret = []
    if(fs.existsSync(dir)) {
        let files = fs.readdirSync(dir)
        console.log(files)
        files.map((item) => {
            let infoJson = {}, infoData = {}
            try{
                infoJson = fs.readFileSync(`${dir}/${item}/pageinfo.json`,'utf-8')
                infoData = JSON.parse(infoJson)
            }catch(err) {
                infoData = {}
            }
            ret.push(new HtmlWebpackPlugin({
                title: infoData.title?infoData.title: 'RABC',
                meta: {
                    keywords: infoData.keywords ? infoData.keywords : 'webpack, react, github',
                    description: infoData.description ?infoData.description : 'webpack, react页面架构'
                },
                chunks: [`${item}`],
                inject: true,
                template: path.resolve(__dirname, '../src/template.html'),
                filename: item == 'index' ? 'index.html':`${item}.html`,
                minify: {
                    collapseWhitespace: true,
                    preserveLineBreaks: true
                }
            }))
        })
    }
    return ret
}