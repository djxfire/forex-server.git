require('babel-register')

const path = require('path')
const webpack = require('webpack')
const entry = require('../utils/entry')
const extra = require('../utils/html-extra')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const ExtraHtmls = extra(path.resolve(__dirname,'../src/pages'))
const entrys = entry(path.resolve(__dirname,'../src/pages'))

module.exports = {
    entry: entrys,
    output:{
        path: path.resolve(__dirname, '../server/static'),
        filename:'[name].js'
    },
    devtool: 'eval-source-map',
    module:{
        rules:[
            {
                test: /\.js$/,
                exclude: /node_module/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            "es2015",
                            "react",
                            "stage-0"
                        ],
                        plugins:[
                            "transform-runtime",
                            "transform-decorators-legacy",
                            "transform-object-rest-spread",
                            ["import", {"libraryName":"antd","style":"css"}]
                        ]
                    }
                }
            },
            {
                test:/\.css$/,
                use: ['style-loader', 'css-loader'],
            //    exclude: /node_module/,
            },
            {
                test:/\.(scss|css)$/,
                use:[
                    "style-loader",
                    {
                        loader:'css-loader',
                        options:{url: false, sourceMap: true},
                    },
                    {
                        loader:'sass-loader',
                        options: {sourceMap: true}
                    }
                ],
                exclude: /node_module/
            },
            // {
            //     test:/\.(less|css)$/,
            //     use:[
            //         "style-loader",
            //         {
            //             loader:'css-loader',
            //             options:{url:false,sourceMap:true}
            //         },
            //         {
            //             loader:'less-loader',
            //             options:{sourceMap:true}
            //         }
            //     ],
            //     exclude:/node_module/
            // },
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader?limit=8192&name=images/[hash:8].[name].[ext]',
                options: {
                    publicPath: '/'
                }
            }
        ]
    },
    devServer: {
      port: 3100,
      open: true
    },
    resolve: {
        alias: {
            components: path.resolve(__dirname, '../src/componetns/'),
            actions: path.resolve(__dirname, '../src/actions/'),
            reducers: path.resolve(__dirname, '../src/reducers/')
        }
    },
    plugins: [
        ...ExtraHtmls,
        new MiniCssExtractPlugin({//分离css插件
            filename: '[name].css',
            chunkFilename: '[id].css'
        }),
        // new webpack.ProvidePlugin({
        //     popper:['popper.js','default']
        // }),
        new webpack.LoaderOptionsPlugin({
            options: {
                optiminzation: {
                    minimizer: [
                        new UglifyJsPlugin({
                            cache: true,
                            parallel: true,
                            sourceMap: false
                        }),
                        new OptimizeCssAssetsPlugin()
                    ],
                    splitChunks: {
                        cacheGroups: {
                            styles: {
                                name: 'styles',
                                test: /\.css$/,
                                chunks: 'all',
                                enforce: true
                            }
                        }
                    }
                }
            }
        })
    ],

}