/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : xue_forex

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 07/03/2021 20:01:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bu_account
-- ----------------------------
DROP TABLE IF EXISTS `bu_account`;
CREATE TABLE `bu_account`  (
  `id` int unsigned NOT NULL,
  `type` tinyint(0) NOT NULL DEFAULT 1,
  `amount` decimal(32, 2) NOT NULL DEFAULT 0.00,
  `occupy` decimal(32, 2) NOT NULL DEFAULT 0.00,
  `user` int(0) NULL DEFAULT NULL,
  `addtime` datetime(0) NULL DEFAULT NULL,
  `remain` decimal(32, 2) NOT NULL DEFAULT 0.00,
  `status` tinyint(0) NOT NULL DEFAULT 1,
  `lever` int(0) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bu_account
-- ----------------------------
INSERT INTO `bu_account` VALUES (1, 0, 100000.00, 0.00, 1, '2021-02-27 19:09:59', 100000.00, 1, 1);
INSERT INTO `bu_account` VALUES (2, 0, 100000.00, 0.00, 2, '2021-02-27 20:39:40', 100000.00, 1, 1);
INSERT INTO `bu_account` VALUES (3, 0, 100000.00, 0.00, 3, '2021-02-27 20:45:56', 100000.00, 1, 1);
INSERT INTO `bu_account` VALUES (4, 0, 100000.00, 0.00, 4, '2021-02-28 22:31:04', 100000.00, 1, 1);

-- ----------------------------
-- Table structure for bu_comment
-- ----------------------------
DROP TABLE IF EXISTS `bu_comment`;
CREATE TABLE `bu_comment`  (
  `id` int unsigned NOT NULL,
  `user` int(0) NOT NULL,
  `post` int(0) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `upvote` int(0) NOT NULL DEFAULT 0,
  `downvote` int(0) NOT NULL DEFAULT 0,
  `addtime` datetime(0) NULL DEFAULT NULL,
  `tocomment` int(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bu_comment
-- ----------------------------
INSERT INTO `bu_comment` VALUES (1, 3, 3, '黄金必涨', 0, 0, '2021-02-28 21:54:47', 0);
INSERT INTO `bu_comment` VALUES (2, 3, 3, '美元放水，比特币大跌，黄金必上2000', 0, 0, '2021-02-28 22:19:17', 0);
INSERT INTO `bu_comment` VALUES (3, 4, 3, '我也觉得黄金能上2000', 0, 0, '2021-02-28 22:31:41', 3);
INSERT INTO `bu_comment` VALUES (4, 4, 2, '测试评论', 0, 0, '2021-02-28 22:35:30', 0);
INSERT INTO `bu_comment` VALUES (5, 4, 4, '消除0评论', 0, 0, '2021-02-28 23:52:18', 0);
INSERT INTO `bu_comment` VALUES (6, 3, 4, '黄金必下1700', 0, 0, '2021-02-28 23:53:14', 0);

-- ----------------------------
-- Table structure for bu_comment_vote
-- ----------------------------
DROP TABLE IF EXISTS `bu_comment_vote`;
CREATE TABLE `bu_comment_vote`  (
  `id` int unsigned NOT NULL,
  `comment` int(0) NOT NULL,
  `user` int(0) NOT NULL,
  `addtime` datetime(0) NULL DEFAULT NULL,
  `vote` char(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bu_limit
-- ----------------------------
DROP TABLE IF EXISTS `bu_limit`;
CREATE TABLE `bu_limit`  (
  `id` int unsigned NOT NULL,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cost` decimal(18, 6) NULL DEFAULT 0.000000,
  `lot` int(0) NULL DEFAULT 0,
  `addtime` datetime(0) NULL DEFAULT NULL,
  `status` tinyint(0) NOT NULL DEFAULT 1,
  `user` int(0) NOT NULL,
  `sellOrBuy` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `account` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bu_order
-- ----------------------------
DROP TABLE IF EXISTS `bu_order`;
CREATE TABLE `bu_order`  (
  `id` int unsigned NOT NULL,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cost` decimal(18, 6) NOT NULL DEFAULT 0.000000,
  `lot` int(0) NOT NULL DEFAULT 0,
  `enob` int(0) NOT NULL DEFAULT 2,
  `amount` decimal(32, 2) NOT NULL DEFAULT 0.00,
  `addtime` datetime(0) NULL DEFAULT NULL,
  `sellOrBuy` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `account` int(0) NOT NULL,
  `user` int(0) NOT NULL,
  `limitid` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bu_post
-- ----------------------------
DROP TABLE IF EXISTS `bu_post`;
CREATE TABLE `bu_post`  (
  `id` int unsigned NOT NULL,
  `user` int(0) NOT NULL,
  `addtime` datetime(0) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `medias` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `commentNum` int(0) NOT NULL DEFAULT 0,
  `upvote` int(0) NOT NULL DEFAULT 0,
  `downvote` int(0) NOT NULL DEFAULT 0,
  `hot` int(0) NOT NULL DEFAULT 0,
  `isTop` tinyint(0) NOT NULL DEFAULT 0,
  `topDay` int(0) NOT NULL DEFAULT 0,
  `topic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bu_post
-- ----------------------------
INSERT INTO `bu_post` VALUES (1, 3, '2021-02-28 02:32:59', '这是测试内容测试内容测试内容', 'upload/20210228/bff6c5a8-4e2a-33ce-638a-3cbb6e6e9333.jpg;upload/20210228/0f32e107-2fed-a130-3ae5-30cca859a612.jpg', 0, 1, 0, 1, 0, 0, 'null');
INSERT INTO `bu_post` VALUES (2, 3, '2021-02-28 02:36:15', '测试机器2测试测试', 'upload/20210228/aa467c33-6ad3-fa76-53a0-50da01156b94.jpg;upload/20210228/3d1ae551-e878-2775-2bc9-477dfe5e9042.jpg;upload/20210228/dbdde023-f82a-0ee8-1859-275322ea0b55.jpg', 1, 2, 0, 4, 0, 0, 'null');
INSERT INTO `bu_post` VALUES (3, 3, '2021-02-28 02:39:07', '测试3333333测试44444444，黄金目标位1720', 'upload/20210228/677442d2-2336-6a7a-5853-b4d65f0eaec3.jpg', 3, 4, 0, 7, 0, 0, 'null');
INSERT INTO `bu_post` VALUES (4, 4, '2021-02-28 23:51:35', '我也来发表一下对黄金的看法，黄金能上2000', 'upload/20210228/22088756-b180-4373-3ccd-0b52b691f1ae.jpg', 2, 1, 0, 3, 0, 0, 'null');

-- ----------------------------
-- Table structure for bu_post_vote
-- ----------------------------
DROP TABLE IF EXISTS `bu_post_vote`;
CREATE TABLE `bu_post_vote`  (
  `id` int unsigned NOT NULL,
  `post` int(0) NOT NULL,
  `user` int(0) NOT NULL,
  `vote` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `addtime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bu_post_vote
-- ----------------------------
INSERT INTO `bu_post_vote` VALUES (7, 3, 3, '1', '2021-02-28 19:01:19');
INSERT INTO `bu_post_vote` VALUES (8, 2, 3, '1', '2021-02-28 19:01:23');
INSERT INTO `bu_post_vote` VALUES (10, 2, 4, '1', '2021-02-28 22:35:11');
INSERT INTO `bu_post_vote` VALUES (11, 3, 4, '1', '2021-02-28 22:35:14');
INSERT INTO `bu_post_vote` VALUES (12, 1, 4, '1', '2021-02-28 23:49:58');
INSERT INTO `bu_post_vote` VALUES (13, 4, 3, '1', '2021-02-28 23:53:36');

-- ----------------------------
-- Table structure for bu_user
-- ----------------------------
DROP TABLE IF EXISTS `bu_user`;
CREATE TABLE `bu_user`  (
  `id` int unsigned NOT NULL,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `tel` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `addtime` datetime(0) NULL DEFAULT NULL,
  `stars` int(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bu_user
-- ----------------------------
INSERT INTO `bu_user` VALUES (3, 'janwool', '123@qq.com', '', 'c1fd7dc5e16ed8210638034683ab906f', 'upload/20210227/144facf6-0727-80d9-6602-5117ba1f6966.jpg', '2021-02-27 20:45:56', 1);
INSERT INTO `bu_user` VALUES (4, 'wulidada', '12345678@qq.com', '', 'c1fd7dc5e16ed8210638034683ab906f', 'upload/20210228/b5e9e56c-3706-ff89-990b-a89cbfa195c5.jpg', '2021-02-28 22:31:04', 2);

-- ----------------------------
-- Table structure for bu_user_star
-- ----------------------------
DROP TABLE IF EXISTS `bu_user_star`;
CREATE TABLE `bu_user_star`  (
  `id` int unsigned NOT NULL,
  `user` int(0) NULL DEFAULT NULL,
  `starUser` int(0) NULL DEFAULT NULL,
  `addtime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bu_user_star
-- ----------------------------
INSERT INTO `bu_user_star` VALUES (3, 4, 3, '2021-02-28 23:48:14');
INSERT INTO `bu_user_star` VALUES (4, 4, 4, '2021-02-28 23:51:57');
INSERT INTO `bu_user_star` VALUES (5, 3, 4, '2021-02-28 23:53:19');

-- ----------------------------
-- Table structure for st_symbol
-- ----------------------------
DROP TABLE IF EXISTS `st_symbol`;
CREATE TABLE `st_symbol`  (
  `id` int unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lineType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prefix` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(0) NULL DEFAULT 1,
  `addtime` datetime(0) NULL DEFAULT NULL,
  `decimal` int(0) NULL DEFAULT 2,
  `secid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of st_symbol
-- ----------------------------
INSERT INTO `st_symbol` VALUES (1, '欧元美元', 'EURUSD', '5d,1m,5m,15m,30m,1d,1w,1M,60m', 'sinapp', 'NewForex', 1, '2021-02-10 23:22:23', 4, '119.EURUSD');
INSERT INTO `st_symbol` VALUES (2, '英镑美元', 'GBPUSD', '1m,5d,5m,15m,30m,60m,4h,1d,1w,1M', 'sinapp', '/forex', 1, '2021-02-15 21:05:46', 4, '119.GBPUSD');
INSERT INTO `st_symbol` VALUES (3, '澳元美元', 'AUDUSD', '1m,5d,5m,15m,30m,60m,4h,1d,1w,1M', 'sinapp', 'NewForex', 1, '2021-02-15 21:06:49', 4, '119.AUDUSD');
INSERT INTO `st_symbol` VALUES (4, '美元日元', 'USDJPY', '1m,5d,5m,15m,30m,60m,4h,1d,1w,1M', 'sinapp', 'NewForex', 1, '2021-02-15 21:08:06', 2, '119.USDJPY');
INSERT INTO `st_symbol` VALUES (5, '美元瑞郎', 'USDCHF', '1m,5d,15m,5m,30m,60m,4h,1d,1w,1M', 'sinapp', 'NewForex', 1, '2021-02-15 21:09:17', 4, '119.USDCHF');
INSERT INTO `st_symbol` VALUES (6, '纽元美元', 'NZDUSD', '1m,5d,5m,15m,30m,60m,4h,1d,1w,1M', 'sinapp', 'NewForex', 1, '2021-02-15 21:10:16', 4, '119.NZDUSD');

-- ----------------------------
-- Table structure for sys_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth`;
CREATE TABLE `sys_auth`  (
  `id` int unsigned NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `adduser` int(0) NOT NULL DEFAULT 0,
  `addtime` bigint(0) NOT NULL DEFAULT 0,
  `modify` int(0) NOT NULL,
  `modifytime` bigint(0) NOT NULL DEFAULT 0,
  `status` tinyint(0) NOT NULL DEFAULT 1 COMMENT '1:有效，2：禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_auth
-- ----------------------------
INSERT INTO `sys_auth` VALUES (1, '超级管理员', '超级管理员拥有所有权限1', 0, 0, 1, 1612108393586, 1);
INSERT INTO `sys_auth` VALUES (3, '测试权限', '', 1, 2147483647, 0, 0, 1);
INSERT INTO `sys_auth` VALUES (4, '测试权限2', '新增数据库权限', 1, 2147483647, 1, 1612108845098, 1);
INSERT INTO `sys_auth` VALUES (8, '测试123456', '测试备注12345', 1, 1612106861366, 1, 1612106861366, 1);

-- ----------------------------
-- Table structure for sys_auth_record
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth_record`;
CREATE TABLE `sys_auth_record`  (
  `id` int unsigned NOT NULL,
  `auth_id` int(0) NOT NULL DEFAULT 0,
  `rule` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `menu_id` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_auth_record
-- ----------------------------
INSERT INTO `sys_auth_record` VALUES (78, 1, NULL, 2);
INSERT INTO `sys_auth_record` VALUES (79, 1, NULL, 4);
INSERT INTO `sys_auth_record` VALUES (80, 1, NULL, 5);
INSERT INTO `sys_auth_record` VALUES (81, 1, NULL, 7);
INSERT INTO `sys_auth_record` VALUES (82, 1, NULL, 12);
INSERT INTO `sys_auth_record` VALUES (83, 1, NULL, 15);
INSERT INTO `sys_auth_record` VALUES (84, 1, NULL, 16);
INSERT INTO `sys_auth_record` VALUES (85, 1, NULL, 17);
INSERT INTO `sys_auth_record` VALUES (86, 1, NULL, 18);
INSERT INTO `sys_auth_record` VALUES (87, 1, NULL, 26);
INSERT INTO `sys_auth_record` VALUES (88, 1, NULL, 27);
INSERT INTO `sys_auth_record` VALUES (89, 1, NULL, 28);
INSERT INTO `sys_auth_record` VALUES (90, 1, NULL, 29);
INSERT INTO `sys_auth_record` VALUES (91, 1, NULL, 30);
INSERT INTO `sys_auth_record` VALUES (92, 1, NULL, 31);
INSERT INTO `sys_auth_record` VALUES (93, 1, NULL, 32);
INSERT INTO `sys_auth_record` VALUES (94, 1, NULL, 33);
INSERT INTO `sys_auth_record` VALUES (95, 1, NULL, 35);
INSERT INTO `sys_auth_record` VALUES (96, 1, NULL, 36);
INSERT INTO `sys_auth_record` VALUES (97, 1, NULL, 37);
INSERT INTO `sys_auth_record` VALUES (98, 1, NULL, 38);
INSERT INTO `sys_auth_record` VALUES (99, 1, NULL, 39);
INSERT INTO `sys_auth_record` VALUES (100, 1, NULL, 40);
INSERT INTO `sys_auth_record` VALUES (101, 1, NULL, 1);
INSERT INTO `sys_auth_record` VALUES (102, 1, NULL, 3);
INSERT INTO `sys_auth_record` VALUES (103, 4, NULL, 31);
INSERT INTO `sys_auth_record` VALUES (104, 4, NULL, 32);
INSERT INTO `sys_auth_record` VALUES (105, 1, NULL, 46);
INSERT INTO `sys_auth_record` VALUES (106, 1, NULL, 47);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int unsigned NOT NULL,
  `pid` int(0) NOT NULL DEFAULT 0,
  `type` tinyint(0) NOT NULL DEFAULT 1 COMMENT '1：菜单，2：按钮，3：接口',
  `status` tinyint unsigned NOT NULL COMMENT '1:有效，2：禁用',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rule` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `data` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort` smallint(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, 1, 1, '系统', 'fa fa-gears', '系统设置', 'sys', '', 0);
INSERT INTO `sys_menu` VALUES (2, 1, 1, 1, '用户管理', 'fa fa-group', '管理员管理', '/user', '', 0);
INSERT INTO `sys_menu` VALUES (3, 1, 1, 1, '菜单管理', 'fa fa-bars', '菜单管理', '/menu', '', 2);
INSERT INTO `sys_menu` VALUES (4, 1, 1, 1, '权限管理', 'fa fa-shield', '权限管理', '/auth', '', 1);
INSERT INTO `sys_menu` VALUES (5, 0, 1, 1, '首页', 'fa fa-fighter-jet', '首页', '/index', '', 0);
INSERT INTO `sys_menu` VALUES (7, 3, 2, 1, '编辑菜单', '#', '编辑菜单', '/menu/edit', '', 0);
INSERT INTO `sys_menu` VALUES (8, 3, 2, 1, '新增菜单', 'fa fa-plus', '新增菜单', '/menu/add', '', 1);
INSERT INTO `sys_menu` VALUES (12, 3, 2, 1, '删除菜单', '#', '', '/menu/del', '', 2);
INSERT INTO `sys_menu` VALUES (15, 4, 2, 1, '新增权限', '#', '新增权限', '/auth/add', '', 2);
INSERT INTO `sys_menu` VALUES (16, 4, 2, 1, '编辑权限', '', '编辑权限', '/auth/edit', '', 3);
INSERT INTO `sys_menu` VALUES (17, 4, 2, 1, '删除权限', '', '删除权限', '/auth/del', '', 4);
INSERT INTO `sys_menu` VALUES (18, 4, 2, 1, '分配权限', '', '分配权限', '/auth/allot', '', 5);
INSERT INTO `sys_menu` VALUES (26, 2, 2, 1, '新增用户', '', '', '/user/add', '', 1);
INSERT INTO `sys_menu` VALUES (27, 2, 2, 1, '编辑用户', '', '', '/user/edit', '', 2);
INSERT INTO `sys_menu` VALUES (28, 2, 2, 1, '分配角色', '', '', '/user/allot', '', 3);
INSERT INTO `sys_menu` VALUES (29, 2, 2, 1, '启用禁用', '', '', '/user/status', '', 4);
INSERT INTO `sys_menu` VALUES (30, 0, 1, 1, '工程管理', '#', '工程管理', 'proj', '', 0);
INSERT INTO `sys_menu` VALUES (31, 30, 1, 1, '数据库', 'fa fa-database', '', '/mdprj/index', '', 1);
INSERT INTO `sys_menu` VALUES (32, 31, 2, 1, '新增数据库', '#', '', '/mdprj/add', '', 1);
INSERT INTO `sys_menu` VALUES (33, 31, 2, 1, '新增文档', '', '', '/mdprj/collection_add', '', 2);
INSERT INTO `sys_menu` VALUES (35, 30, 1, 1, '工程流程', 'fa fa-code-fork', '', '/procedure/index', '', 2);
INSERT INTO `sys_menu` VALUES (36, 35, 2, 1, '新增流程', '#', '', '/procedure/add', '', 1);
INSERT INTO `sys_menu` VALUES (37, 35, 2, 1, '设计流程', '#', '', '/procedure/design', '', 3);
INSERT INTO `sys_menu` VALUES (38, 35, 2, 1, '添加流程节点', '#', '', '/procedure/add_node', '', 4);
INSERT INTO `sys_menu` VALUES (39, 35, 2, 1, '获取流程节点', '#', '', '/procedure/get_nodes', '', 4);
INSERT INTO `sys_menu` VALUES (40, 35, 2, 1, '连接流程节点', '#', '', '/procedure/node_connection', '', 5);
INSERT INTO `sys_menu` VALUES (42, 41, 1, 1, '请假管理', '', '', '/app/index', 'p=ceshiliucheng&n=shangjizhuguanshenhe', 0);
INSERT INTO `sys_menu` VALUES (43, 42, 2, 1, '新增请假单', '#', '', '/app/forms', 'p=ceshiliucheng&n=shangjizhuguanshenhe&b=xinzeng', 1);
INSERT INTO `sys_menu` VALUES (45, 44, 1, 1, '外发单', 'fa fa-child', '', '/app/index', 'p=teshugongyi&n=waifashenqing', 1);
INSERT INTO `sys_menu` VALUES (46, 0, 1, 1, '合约管理', '', 'null', 'symbol', '', 2);
INSERT INTO `sys_menu` VALUES (47, 46, 1, 1, '外汇', '', 'null', '/symbol', 'type=forex', 3);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int unsigned NOT NULL,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `valid` tinyint(0) NOT NULL DEFAULT 1 COMMENT '1:有效，2：禁用',
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adduser` int(0) NOT NULL DEFAULT 0,
  `modifyuser` int(0) NOT NULL DEFAULT 0,
  `addtime` int(0) NOT NULL,
  `modifytime` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'administrator', 'd937abb78ad3bbd10e0cc686ae494a6d', 1, '516385822@qq.com', 0, 0, 0, 0);
INSERT INTO `sys_user` VALUES (2, 'test111', '!a123456', 1, 'janwoolx45@163.com', 1, 0, 2147483647, 0);

-- ----------------------------
-- Table structure for sys_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_auth`;
CREATE TABLE `sys_user_auth`  (
  `id` int unsigned NOT NULL,
  `auth_id` int(0) NOT NULL,
  `user_id` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_auth
-- ----------------------------
INSERT INTO `sys_user_auth` VALUES (1, 1, 1);
INSERT INTO `sys_user_auth` VALUES (4, 4, 2);
INSERT INTO `sys_user_auth` VALUES (5, 3, 2);

SET FOREIGN_KEY_CHECKS = 1;
